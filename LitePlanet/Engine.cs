﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiteEngine.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Controllers;
using LiteEngine.Physics;
using LiteEngine.Input;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Particles;
using LiteEngine.Rendering;
using LiteEngine.Fov;
using LitePlanet.Worlds;
using LitePlanet.Vessels;
using LitePlanet.Effects;
using LitePlanet.Weapons;
using LitePlanet.Projectiles;
using LitePlanet.AI;
using LitePlanet.Maps;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using LitePlanet.UI;
using LitePlanet.Comms;
using LiteEngine.Textures;
using LitePlanet.Structures;
using LitePlanet.Creatures;

namespace LitePlanet
{
    public class Engine : LiteXnaEngine
    {
        Camera2D _camera;
        LiteEngine.Textures.Texture _starsTexture = new LiteEngine.Textures.Texture("stars");
        LiteEngine.Textures.Texture _planetTexture = new LiteEngine.Textures.Texture("planet");
        LiteEngine.Textures.Texture _pointTexture = new LiteEngine.Textures.Texture("point");
        LiteEngine.Textures.Texture _particleTexture = new LiteEngine.Textures.Texture("particle");

        ParticlePool _exhaustParticles;
        ParticlePool _smokeParticles;

        PlayerInfo _playerInfo;

        Carrier _carrier;
        Bullets _bullets;
        Shells _shells;

        Hud _hud;
        int _money = 300;
        List<Ship> _aiShips = new List<Ship>();
        List<Pilot> _aiPilots = new List<Pilot>();
        Dock _dock;
        StarSystem _system;
        SystemMap _systemMap;

        TargettingModule _targetter = new TargettingModule();

        List<Missile> _missiles = new List<Missile>();

        public Engine()
        {
            Globals.Engine = this;
        }

        internal PlayerInfo PlayerInfo
        {
            get
            {
                return _playerInfo;
            }
        }

        protected override void Initialize(XnaRenderer renderer)
        {
            renderer.SetResolution(800, 600, true);
            UserInterface.SetResolution(600, 480);
            _camera = new Camera2D(new Vector2(0, 0), new Vector2(80, 60));

            TextureBook.AddSpriteSheet(@"textures\planet1");

            Physics.SetGlobalGravity(new Vector2(0f, 0f));

            _system = new StarSystem(Physics);

            //foreach (Planet planet in _system.Planets)
            //    gc.AddPoint(planet.Position);

            _exhaustParticles = ParticleSystem.CreateParticleFactory();
            _smokeParticles = ParticleSystem.CreateParticleFactory();

            _bullets = new Bullets(this);
            _shells = new Shells(this);

            _playerInfo = new PlayerInfo();

            _playerInfo.Ship = new LegoShip(this); // new Carrier(this); // Ship(this);
            ShipLayout layout = _playerInfo.Ship.Layout;
            layout.Initialize(3, 4);
            layout.RemoveCell(0, 0);
            layout.RemoveCell(2, 0);
            layout.RemoveCell(0, 1);
            layout.RemoveCell(2, 1);

            layout.InstallComponent(1, 3, new SmokerEngine());
            layout.InstallComponent(1, 0, new Blaster());
            layout.InstallComponent(1, 2, new FuelReactor());

            _playerInfo.Ship.Position = _system.Planets[0].Position - new Vector2(0, _system.Planets[0].Radius + 25);
            _playerInfo.Ship.Rotation = (float)Math.PI;

            _playerInfo.Money = 3000;

            TargetInfo info = new TargetInfo();
            info.Name = "Your ship";
            _playerInfo.Ship.SetInfo(info);

            _system.AddShip(_playerInfo.Ship);

            _systemMap = new SystemMap(_system);


            GravityController gc = new GravityController(5, 5000, 1);
            gc.GravityType = GravityType.Linear;
            gc.AddPoint(_system.Planets[0].Position);
            //gc.Enabled = true;
            //Physics.World.AddController(gc);

            //TODO: Could Fixture.AfterCollision event be used?


            //_building = new Building(this, new Vector2(-10, 22), 6, 6);
            //_building2 = new Building(this, new Vector2(10, 22), 6, 6);

            for (int i = 0; i < 1; i++)
            {
                _carrier = new Carrier(this);
                info = new TargetInfo();
                info.Name = "Green Giant";
                info.Type = "Supply Ship";
                info.CanDock = true;
                _carrier.SetInfo(info);

                _carrier.Position = new Vector2(_playerInfo.Ship.Position.X + i * 20, _playerInfo.Ship.Position.Y - 100);
                //pilot = new Pilot(_carrier);
                _aiShips.Add(_carrier);
                //_aiPilots.Add(pilot);
                _system.AddShip(_carrier);
            }

            _playerInfo.Ship.Position = _carrier.Position + new Vector2(10, 100);

            //_dock = new Dock(this);
            //_dock.Position = new Vector2(-20, 7);

            for (int i = 0; i < 2; i++)
            {
                float angle = Dice.Next() * (float)Math.PI * 2f;
                Vector2 orbitPosition = _carrier.Position + new Vector2((float)Math.Sin(angle), -(float)Math.Cos(angle)) * 40;
                Pilot pilot = CreateShip(orbitPosition, "Patrol ship #" + i, "Fighter", false);
                pilot.Circle(_carrier);
            }

            _hud = new Hud(this, _playerInfo, UserInterface.Size);
            UserInterface.AddChild(_hud);

            _cameraTarget = _playerInfo.Ship;

            //BuildPanel buildPanel = new BuildPanel(this);
            //buildPanel.Size = new SizeF(100, UserInterface.Bounds.Height);
            //buildPanel.Dock = DockPosition.Right;
            //UserInterface.AddChild(buildPanel);

            UserInterface.ShowMouseCursor = true;
        }

        internal Pilot CreateShip(Vector2 position, string name, string type, bool hostile)
        {
            Ship ship = new Ship(this, hostile);
            ship.MaxSpeed = 10;
            ship.Position = position;
            ship.Body.Rotation = 1f;
            TargetInfo info = new TargetInfo();
            info.Name = name;
            info.Type = type;
            ship.SetInfo(info);
            Pilot pilot = new Pilot(ship);
            _aiPilots.Add(pilot);
            _system.AddShip(ship);
            return pilot;
        }

        bool _mapMode = false;

        public ParticlePool SmokeParticles
        {
            get
            {
                return _smokeParticles;
            }
        }

        public ParticlePool ExhaustParticles
        {
            get
            {
                return _exhaustParticles;
            }
        }

        public Bullets Bullets
        {
            get
            {
                return _bullets;
            }
        }

        internal Shells Shells
        {
            get
            {
                return _shells;
            }
        }

        public StructureType SelectedBuildStructureType = StructureType.None;

        protected override int OnKeyPress(Keys key, GameTime gameTime)
        {
            if (_mapMode)
            {
                if (key == Keys.Escape || key == Keys.M)
                {
                    _mapMode = false;
                    return 10;
                }
                return _systemMap.OnKeyPress(key);
            }

            KeyPressResult res = _hud.ProcessKey(UserInterface, key);
            if (res.Handled)
                return res.ReprocessDelay;

            switch (key)
            {
                case Keys.Escape:
                    Exit();
                    break;
                case Keys.O:
                    _carrier.LaunchShip(_playerInfo.Ship);
                    _hud.MessageWindow.AddMessage(Message.FromEvent("Leaving the " + _carrier.Info.Name));
                    return -1;
                case Keys.K:
                    delete = true;
                    return -1;
                case Keys.T:
                    _hud.MessageWindow.AddMessage(Message.FromEvent("[[This is a short test event message]]"));
                    return -1;
                case Keys.Y:
                    //_hud.MessageWindow.AddMessage(Message.FromCommunication("Sender Name [Sender Type]", "This is a long communication message that will wrap round onto the next line as it contains so many words it won't fit on a single line."));
                    return -1;
                case Keys.P:
                    _carrier.SendMessage("ATTENTION - IMCOMING PIRATES. TAKE SHELTER IMMEDIATELY.");
                    for (int i = 0; i < 2; i++)
                    {
                        Vector2 position = _carrier.Position + new Vector2(200 + i, i * 6f); ;
                        Pilot pilot = CreateShip(position, "Pirate #" + i, "Fighter", true);
                        pilot.Ship.Rotation = (float)Math.PI * 1.5f;
                    }
                    return -1;
                case Keys.Tab:
                    _targetter.NextTarget(_system, _playerInfo.Ship.Position, _playerInfo.Ship);
                    //_hud.SelectionInfoView.SetSelection(_targetter.CurrentTarget);
                    return -1;
                case Keys.W:
                    if (!_playerInfo.Ship.IsDocked)
                        _playerInfo.Ship.FireForwardEngines(this);
                    break;
                case Keys.L:
                    _playerInfo.Ship.ApplyForwardThrust(-0.001f);
                    break;
                case Keys.A:
                    _playerInfo.Ship.ApplyRotateThrust(-0.081f);
                    break;
                case Keys.D:
                    _playerInfo.Ship.ApplyRotateThrust(0.081f);
                    break;
                case Keys.M:
                    _mapMode = !_mapMode;
                    ShowSystemMap(_mapMode);
                    return 10;
                case Keys.N:
                    if (CameraTarget == null)
                        CameraTarget = _playerInfo.Ship;
                    else
                        CameraTarget = null;
                    return 10;
                //case Keys.J:
                //    //float altitude = _ship.Position.Length() - _planet.Radius;
                //    return 10;
                case Keys.Z:
                    _camera.Zoom *= 1.05f;
                    break;
                case Keys.X:
                    float zoom = _camera.Zoom * 0.95f;
                    if (zoom < 0.2f)
                        zoom = 0.2f;
                    _camera.Zoom = zoom;
                    break;
                case Keys.Space:
                    _playerInfo.Ship.Fire(this);
                    break;
            }
            return 0;
        }

        private void ShowSystemMap(bool show)
        {
            if (show)
            {
                _systemMap.Origin = _system.Planets[0];
            }
        }

        public void CreateMissile(Vector2 position, Vector2 velocity)
        {
            Missile missile = new Missile(Physics);
            missile.Body.Position = position;
            missile.Body.LinearVelocity = velocity;
            missile.SetTarget(_playerInfo.Ship);
            _missiles.Add(missile);
        }

        internal List<Bee> Bees = new List<Bee>();

        protected override void UpdateFrame(GameTime gameTime)
        {
            _system.Planets[0].CollisionField.Begin();

            _shells.UpdateCollisionFields(_system.Planets[0].CollisionField);

            _system.Planets[0].CollisionField.CreateCollisionTilesAroundPoint(_playerInfo.Ship.Position);

            _system.Planets[0].CollisionField.End();

            foreach (Missile missile in _missiles)
                missile.Think();

            foreach (Bee bee in Bees)
            {
                _system.Planets[0].CollisionField.CreateCollisionTilesAroundPoint(bee.Body.Position);
                bee.Think();
            }

            foreach (Creature creature in _system.Planets[0].Biosphere.ActiveRegion.Creatures)
            {
                _system.Planets[0].CollisionField.CreateCollisionTilesAroundPoint(creature.Position);
            }

            foreach (Planet planet in _system.Planets)
            {
                if (planet.FarView.TileTexture != null)
                    planet.Think(this);


                Vector2 diff = _playerInfo.Ship.Position - planet.Position;
                float altitude = diff.Length() - planet.Radius;
                if (altitude > 50)
                    continue;

                Vector2 eye = planet.CartesianToPolar(_playerInfo.Ship.Position);
                if (planet.FarView.TileTexture != null)
                    planet.Fov.RunFov(planet, (int)Math.Floor(eye.X), (int)Math.Floor(eye.Y), 30);
                //planet.CollisionFieldGenerator.UpdateFields();
            }

            foreach (Pilot p in _aiPilots)
            {
                
                p.Update(this);
                //p.Target(this, _playerInfo.Ship);
            }

            foreach (Ship ship in _system.Ships.ToArray())
            {
                if (ship.Hull <= 0)
                {
                    _system.RemoveShip(ship);
                }
            }

            //foreach (Ship s in _aiShips)
            //{
            //    Carrier carrier = s as Carrier;
            //    if (carrier != null)
            //        carrier.Target(_playerInfo.Ship);
            //}

            _playerInfo.Ship.Update();

            _system.Update();

            _system.Planets[0].Biosphere.ActiveRegion.Center = _camera.Position;
            _system.Planets[0].Biosphere.ActiveRegion.FindNewCreatures();

            _hud.Update(_frame);
        }

        internal StarSystem System
        {
            get
            {
                return _system;
            }
        }

        Planet GetNearPlanet(Vector2 position)
        {
            foreach (Planet planet in _system.Planets)
            {
                Vector2 diff = position - planet.Position;
                float altitude = diff.Length() - planet.Radius;
                if (altitude < 70)
                    return planet;
            }
            return null;
        }

        internal void DrawSun(XnaRenderer renderer)
        {
            Color color = Color.Yellow;
            color *= 0.02f;
            for (float f = 10000; f > 2; f -= 100f)
            {
                float n = f + Dice.Next() * 100f;
                renderer.DrawSprite(_pointTexture, new Vector2(20, 0), new Vector2(n, n), 0, color, 0f);
            }

            color = Color.Orange;
            color *= 0.03f;
            for (float f = 10200; f > 2; f -= 300f)
            {
                float n = f;// +Dice.Next() * 2f;
                renderer.DrawSprite(_pointTexture, new Vector2(20, 0), new Vector2(n, n), 0, color, 0f);
            }

            color = Color.White;
            color *= 0.1f;
            for (float f = 10000; f > 2; f -= 500f)
            {
                float n = f +Dice.Next() * 200f;
                renderer.DrawSprite(_pointTexture, new Vector2(20, 0), new Vector2(n, n), 0, color, 0f);
            }
        }

        ITargetable _cameraTarget;
        internal ITargetable CameraTarget
        {
            get
            {
                return _cameraTarget;
            }
            set
            {
                _cameraTarget = value;
            }
        }

        PlanetTile SelectedTile
        {
            get
            {
                Vector2 mousePosition = _camera.ViewToWorld(GetMousePosition());
                mousePosition = _system.Planets[0].CartesianToPolar(mousePosition);
                if (mousePosition.X < 0)
                    mousePosition.X += _system.Planets[0].Width;
                return _system.Planets[0].GetTile((int)(mousePosition.X + 0f), (int)(mousePosition.Y + 0f));
            }
        }

        Colony _colony = new Colony();
        public Colony Colony
        {
            get
            {
                return _colony;
            }
        }

        bool delete = false;
        protected override void OnMouseClick(MouseButton button, Vector2 mousePosition)
        {
            PlanetTile tile = SelectedTile;
            if (tile == null)
                return;

            if (delete)
            {
                if (tile.Structure != null)
                    tile.Structure.Remove();
                delete = false;
                return;
            }

            if (button == MouseButton.Left && SelectedBuildStructureType != StructureType.None)
            {



                _colony.Build(SelectedBuildStructureType, tile);

            }
            //tile.TakeDamage(5);
        }

        BuildPlacementLayout _placementLayout = new BuildPlacementLayout();

        Vector2 MousePosition
        {
            get
            {
                
                return _camera.ViewToWorld(GetMousePosition());
            }
        }

        float _angle = 0;
        Vector2 _lookAt = new Vector2(0, 500);
        Vector2 _lookAtVel = new Vector2(0, 0);

        Planet _nearPlanet;
        int _frame = 0;
        protected override void DrawFrame(GameTime gameTime, XnaRenderer renderer)
        {
            renderer.Clear(Color.Black);

            if (_mapMode)
            {
                _systemMap.Draw(renderer);
                return;
            }

            if (_nearPlanet != null)
                if (_nearPlanet.Altitude(_playerInfo.Ship.Position) > 70)
                    _nearPlanet = null;

            if (_nearPlanet == null)
                _nearPlanet = GetNearPlanet(_playerInfo.Ship.Position);
            float angle = (float)Math.Atan2(_playerInfo.Ship.Position.X - _system.Planets[0].Position.X, -_playerInfo.Ship.Position.Y - _system.Planets[0].Position.Y);

            //Vector2 shipPos = _system.Planets[0].CartesianToPolar(_playerInfo.Ship.Position);

            //if (shipPos.X + _lookAtVel.X * 10 > _lookAt.X + 5)
            //    _lookAtVel.X += 0.001f;
            //if (shipPos.X + _lookAtVel.X * 10 < _lookAt.X - 5)
            //    _lookAtVel.X -= 0.001f;

            //_lookAt += _lookAtVel;

            //_camera.LookAt(_system.Planets[0].PolarToCartesian(_lookAt), _angle);

            if (_cameraTarget != null)
            {
                //if (_playerInfo.Ship.IsDocked)
                //    cameraTarget = _playerInfo.Ship.DockedWith;
                _camera.LookAt(_cameraTarget.Location, angle);
            }

            
            DrawStars(renderer);

            renderer.BeginDraw(_camera);

            foreach (Planet planet in _system.Planets)
            {
                planet.Draw(renderer, _camera, gameTime);
                break;
            }

            if (SelectedBuildStructureType != StructureType.None && SelectedTile != null)
            {
                renderer.DrawSprite(StructureFactory.GetIcon(SelectedBuildStructureType), SelectedTile.Corners, Color.FromNonPremultiplied(new Vector4(1,1,1, 0.6f)));
                //renderer.DrawSprite(new LiteEngine.Textures.Texture("border"), SelectedTile.Corners, Color.Green);
            }
            
            if (_placementLayout != null && SelectedTile != null)
            {
                _placementLayout.Validate(SelectedTile);
                _placementLayout.DrawOverlay(renderer, SelectedTile);
            }


            //var tile = SelectedTile;
            //if (tile != null && SelectedBuildStructureType != null)
            //{
            //    bool canPlace = _colony.CanBuild(SelectedBuildStructureType, tile);
            //    Color color = Color.Red;
            //    if (canPlace)
            //        color = Color.Green;
            //    tile.DrawHighlight(renderer, color);
            //}

            //if (_buildSelection != null)
            //{
            //    _buildSelection.Draw(renderer);
            //}
            //else
                //renderer.DrawPoint(MousePosition, 1f, Color.White, 1f);
            //_carrier.DrawCollisionArea(renderer);

            

            foreach (Ship s in _system.Ships)
            {
                //don't draw docked ships
                if (s.IsDocked)
                    continue;
                s.Draw(renderer);
                if (_camera.Zoom > 2)
                    s.DrawIcon(renderer, _camera.Zoom);

                if (_targetter.CurrentTarget == s)
                {
                    if (!_playerInfo.Ship.IsDocked)
                        s.DrawDockArrows(renderer, false);
                    if (_frame % 30 < 15)
                        s.DrawIcon(renderer, _camera.Zoom, true);
                }
            }

            if (_playerInfo.Ship.IsDocked)
            {
                _playerInfo.Ship.DockedWith.DrawDockArrows(renderer, true);
            }

            _frame++;

            if (_nearPlanet != null)
            {
                foreach (Item item in _nearPlanet.Items)
                    item.Draw(renderer);
            }
            DrawSun(renderer);

            renderer.DrawDepth = 0.3f;
            foreach (Particle p in _exhaustParticles.Particles)
            {
                float particleSize = 0.5f * (p.Life / 50f);
                float alpha = (float)p.Life * p.Life / (50 * 50);
                Color color = new Color(1, 1, (float)p.Life / 60f);
                p.Draw(renderer, particleSize, color, 0);
            }

            foreach (Particle p in _smokeParticles.Particles)
            {
                float particleSize = 1.4f;
                float alpha = (float)p.Life * p.Life / (50 * 50);
                float c = (float)p.Life / 100;
                Color color = new Color(c, c, c);
                p.Draw(renderer, particleSize, color, alpha);
            }

            foreach (Particle p in _bullets.Particles)
            {
                float alpha = 0f;
                //Color color = Color.Lerp(Color.Cyan, Color.Blue, Dice.Next());
                Color color = Color.Lerp(Color.LightPink, Color.Red, Dice.Next());


                Vector2 size = new Vector2(0.3f, 2);
                float rotation = Util.AngleBetween(new Vector2(0, 1), p.Velocity);
                p.Draw(renderer, size, color, alpha, rotation);

                size *= 1.2f;
                p.Draw(renderer, size, color, alpha, rotation);

                size *= 1.2f;
                p.Draw(Renderer, size, Color.White, alpha, rotation);
            }

            _shells.Draw(renderer);

            //_building.Draw(renderer);
            //_building2.Draw(renderer);

            foreach (Missile missile in _missiles)
                renderer.DrawPoint(missile.Body.Position, 1f, Color.Red, 1f);

            foreach (Bee bee in Bees)
                bee.Draw(renderer);

            renderer.EndDraw();


            renderer.BeginDraw(UserInterface.Camera);

            if (_targetter.CurrentTarget != null)
            {
                _hud.UpdateSelectionOverlay(renderer, _camera, this, _targetter.CurrentTarget);
            }

            string frameRate = FrameRate + " FPS";
            renderer.DrawStringBox(frameRate, new RectangleF(200, 10, 120, 10), Color.White);

            renderer.EndDraw();

            

            //renderer.DrawStringBox(_ship.Position.X + ", " + _ship.Position.Y, new RectangleF(11, 71, 200, 10), Color.White);
            
            //altitude = _ship.Position.Length() - planetToDraw.Radius;
            //float zoom = 1;
            //if (altitude > 25)
            //    zoom = altitude / 25;
 
            //Renderer.DrawStringBox("Altitude: " + (_ship.Position.Length() - _planet.Radius), new RectangleF(11, 71, 200, 10), Color.White);
            
            //if (_ship.JumpDriveCharging)
            //    Renderer.DrawStringBox("Jump Drive Charging (destination: " + _systemMap.Target.Name + "): " + _ship.JumpDriveCharge.ToString("0.00") + "%", new RectangleF(11, 91, 500, 10), Color.Red);
        }

        private void DrawStars(XnaRenderer renderer)
        {
            Camera2D camera = new Camera2D(new Vector2(0.5f, 0.5f), new Vector2(1, 1));
            renderer.BeginDraw(camera);
            renderer.DrawSprite(_starsTexture, new RectangleF(0, 0, 1, 1), 0.2f, 0, new Vector2(0f, 0f), Color.White, false, false);
            renderer.EndDraw();
        }

        internal TargettingModule Targetter
        {
            get
            {
                return _targetter;
            }
        }
    }

    class Dock
    {
        static LiteEngine.Textures.Texture _texture = new LiteEngine.Textures.Texture("building");
        Body _body;

        public Dock(Engine engine)
        {
            _body = engine.Physics.CreateRectangleBody(null, 4f, 20f, 1);
            _body.IsStatic = true;
        }

        public void Update()
        {
            float delta = -0.03f;
            Position = new Vector2(Position.X, Position.Y + delta);
        }

        public Vector2 Position
        {
            get
            {
                return _body.Position;
            }
            set
            {
                _body.Position = value;
            }
        }

        public void Draw(XnaRenderer renderer)
        {
            for (int y=0;y<5;y++)
                renderer.DrawSprite(_texture, new RectangleF(Position.X, Position.Y + y * 4, 4, 4), 0);
        }
    }
}
