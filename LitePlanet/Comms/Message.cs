﻿using LitePlanet.ShipComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Comms
{
    /// <summary>
    /// Encapsulates a message from a component or another ship that appears in the message log
    /// </summary>
    class Message
    {
        ITargetable _sender;
        string _text;
        MessageType _type;
        public Message(string text, MessageType type, ITargetable sender = null)
        {
            _text = text;
            _type = type;
            _sender = sender;
        }

        public static Message FromCommunication(ITargetable sender, string text)
        {
            return new Message(text, MessageType.Communication, sender);
        }

        public static Message FromEvent(string text)
        {
            return new Message(text, MessageType.Event, null);
        }

        public int Age
        {
            get;
            set;
        }

        public ITargetable Sender
        {
            get
            {
                return _sender;
            }
        }

        public string Text
        {
            get
            {
                return _text;
            }
        }

        public MessageType Type
        {
            get
            {
                return _type;
            }
        }
    }

    enum MessageType
    {
        Communication,
        Event
    }
}
