﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using LitePlanet.Vessels;
using LiteEngine.Math;
using LiteEngine.Core;

namespace LitePlanet.AI
{
    class Pilot
    {
        Ship _ship;
        public Pilot(Ship ship)
        {
            _ship = ship;
        }

        public Ship Ship
        {
            get
            {
                return _ship;
            }
        }

        public void RotateToFace(float angle)
        {
            float targetHeading = angle;
            float heading = _ship.Rotation + _ship.Body.AngularVelocity * 0.1f;
            float angleDist = Util.AngleBetween(heading, targetHeading);

            float thrust = 0.003f;// 81f; //0.2f;
            if (_ship.Body.Mass > 1)
                thrust = 10f;

            if (angleDist > 0.01f)
                _ship.ApplyRotateThrust(thrust);
            else if (angleDist < -0.01f)
                _ship.ApplyRotateThrust(-thrust);
        }

        int s = 0;

        internal void Target(Engine engine, Ship target)
        {
            if (_ship.Hull <= 0)
                return;

            Vector2 aimPoint = target.Position + target.Velocity * LiteEngine.Core.Dice.Next() * 1;
            float dist = Vector2.Distance(target.Position, _ship.Position);
            float angle = Util.AngleBetween(_ship.Position, aimPoint);
            RotateToFace(angle);

            if (s > 0)
                s--;
            if (dist < 40 && s == 0)
            {
                if (Math.Abs(Util.AngleBetween(_ship.Rotation, angle)) < 0.2f)
                    _ship.PrimaryWeapon.Fire(engine, _ship, _ship.Position, _ship.Facing);
                s = 5;
            }
            else
            {
                GoTo(target.Position);
            }
        }

        internal void GoTo(Vector2 destination)
        {
            float dist = Vector2.Distance(destination, _ship.Position);
            Vector2 v = destination - (_ship.Position + _ship.Velocity*2f);
            float angle = (float)Math.Atan2(v.X, -v.Y);

            RotateToFace(angle);

            if (Math.Abs(Util.AngleBetween(_ship.Rotation, angle)) < 0.3f)
                _ship.ActivateEngines(1);
            else
                _ship.ActivateEngines(0);
        }

        Ship _target;
        TargetBehavior _behavior;

        enum TargetBehavior
        {
            Idle,
            Circle,
            Attack
        }

        internal void Circle(Ship target)
        {
            _target = target;
            _behavior = TargetBehavior.Circle;
        }

        internal void Attack(Ship target)
        {
            _target = target;
            _behavior = TargetBehavior.Attack;
        }

        internal void Update(Engine engine)
        {
            if (_behavior != TargetBehavior.Attack)
            {
                //find nearest target
                Ship randomShip = engine.System.Ships[Dice.Next(engine.System.Ships.Count)];
                if (Ship.Info.Name.Contains("Patrol") && randomShip.Info.Name.Contains("Pirate")
                    || (Ship.Info.Name.Contains("Pirate") && !randomShip.Info.Name.Contains("Pirate")))
                {
                    Attack(randomShip);
                    return;
                }

                if (Ship.Info.Name.Contains("Patrol") && randomShip.Info.Name.Contains("Green"))
                {
                    Circle(randomShip);
                    return;
                }
            }

            if (_behavior == TargetBehavior.Circle)
            {
                Vector2 diff = _ship.Position - _target.Position;
                float angle = (float)Math.Atan2(diff.X, -diff.Y);
                angle -= 0.5f;
                Vector2 dest = _target.Position + new Vector2((float)Math.Sin(angle), (float)-Math.Cos(angle)) * 50f;
                GoTo(dest);
            }

            if (_behavior == TargetBehavior.Attack)
            {
                Target(engine, _target);
                if (_target.Hull <= 0)
                {
                    _target = null;
                    _behavior = TargetBehavior.Idle;
                }
            }
        }
    }
}
