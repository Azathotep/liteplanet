struct TileType
{
   int TextureId;
   float3 ColorStart;
   float3 ColorEnd;
};

struct PlanetVsOut
{
    float4 Position   	: SV_POSITION;
    float4 Color	: COLOR;
    float3 WorldPos	: TEXCOORD0;
};

struct PsOut
{
    float4 Color : COLOR;
};

static const float PI = 3.14159265f;


float4x4 xView;
float4x4 xProjection;
float4x4 xWorld;
float zoom;
float2 planetPos;
float wav;
int planetWidth;

//------- Texture Samplers --------

Texture2D xTexture;
Texture2D xTilesTexture;

SamplerState TextureSampler
{ 
  Texture = <xTexture>; 
  Filter = MIN_MAG_MIP_POINT;
  AddressU = mirror; AddressV = mirror;
};

//MIN_MAG_LINEAR_MIP_POINT;
//MIN_MAG_MIP_POINT
//MIN_MAG_MIN_LINEAR

//------- Technique: Planet --------

PlanetVsOut PlanetVS( float4 inPos : SV_POSITION, float4 inColor: COLOR)
{	
	PlanetVsOut ret = (PlanetVsOut)0;
	float4x4 viewProjection = mul (xView, xProjection);
	float4x4 worldViewProjection = mul (xWorld, viewProjection);
    
	ret.Position = mul(inPos, worldViewProjection);	
	ret.WorldPos = mul(inPos, xWorld);
        ret.Color = inColor;
	return ret;    
}

PsOut PlanetPS(PlanetVsOut v) 
{
        PsOut ret = (PsOut)0;

        float2 relC = v.WorldPos - planetPos;
	float y = length(relC);

	float angle = atan2(-relC.x, relC.y) + PI;

	float anglePerTile = 2*PI / planetWidth;

	float x = angle / anglePerTile;

        int tileX = floor(x);
        int tileY = floor(y);
        float tileDx = x - tileX;
        float tileDy = (1 - (y - tileY));

        ret.Color = float4(0,0,0,0);

	ret.Color = xTexture.Load(int3(tileX,tileY,0));
	float i = ret.Color.r;
	float a = ret.Color.a;

	bool visible;
        if (a > 0.05)
           visible = true;
        else
	   visible = false;

	//if (fovOffset >= 0 && fovOffset < 100)
		//if (aa[fovOffset] != 1)
		//	visible = false;


        //if (zoom > 30)
        //  return ret;

	//calculate texture x and y for this tile	
	float tx = ret.Color.g;
	float ty = ret.Color.b;

	//float nn = (tileDx * 0.23 + (wav * 0.5f + 0.5f) * 0.23f) % 0.23f;

	float w = 0.1345; //(32 / 238)    32=sprite width  238=spritesheet width

	tx = tx + tileDx * w;
    ty = ty + tileDy * w;

	ret.Color = xTilesTexture.Sample(TextureSampler, float2(tx,ty));

	

	/*float4 c = xTilesTexture.Sample(TextureSampler, float2(tx - 0.25f,ty));
	ret.Color.rgb = ret.Color.rgb * (1 - c.a) + c.rgb * c.a;*/
	if (visible)
        {
           return ret;
	}
        ret.Color = float4(0,0,0,1);

	if (y > 500)
          ret.Color = float4(0,0,0,0);
	return ret;
}

technique Planet
{
	pass Pass0
	{   
		VertexShader = compile vs_4_0 PlanetVS();
		PixelShader  = compile ps_4_0 PlanetPS();
	}
}

