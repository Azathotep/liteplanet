﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LiteEngine.Physics;
using LitePlanet.Vessels;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Weapons
{
    class Missile : IPhysicsObject
    {
        Body _body;

        public Missile(PhysicsCore physics)
        {
            _body = physics.CreateBody(this);
            _body.BodyType = BodyType.Dynamic;
            _body.AngularDamping = 0.5f;
            _body.Friction = 1f;
            _body.Restitution = 1.1f;
            _body.Mass = 0.5f;
            _body.Rotation = 0f;
            FixtureFactory.AttachCircle(0.1f, 1f, _body);
            _body.CollisionCategories = Category.Cat2;
            _body.CollidesWith = Category.Cat1 | Category.Cat2 | Category.Cat3;
        }

        public void Think()
        {
            Vector2 idealVelocity = _target.Position - _body.Position;
            idealVelocity.Normalize();
            idealVelocity *= 20f;

            Vector2 velocity = _body.LinearVelocity;

            Vector2 velChange = idealVelocity - velocity;

            _body.ApplyForce(velChange * 0.05f);
        }

        public Body Body
        {
            get { return _body; }
        }

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {
            
        }

        Ship _target;
        internal void SetTarget(Ship target)
        {
            _target = target;
        }
    }
}
