﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.Comms;
using LitePlanet.ShipComponents;
using LitePlanet.Vessels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class Hud : BaseUIControl
    {
        Engine _engine;
        PlayerInfo _playerInfo;
        TextBox _txtStatus;
        MessageWindow _messageWindow;
        SelectionInfoView _selectionInfoView;
        TextBox _selectionOverlay;
        ShipDialog _shipView;
        ShipDockView _dockView;

        TextBox _txtPower;
        TextBox _txtPopulation;
        TextBox _txtResupply;
        TextBox _txtOre;

        public Hud(Engine engine, PlayerInfo playerInfo, SizeF size)
        {
            _engine = engine;
            _playerInfo = playerInfo;
            Size = size;
            KeyboardFocus = false;
            //PowerBar bar = new PowerBar(playerInfo);
            //PowerBar powerBar = new PowerBar(playerInfo);
            //AddChild(powerBar, new RectangleF(10, 20, 20, Bounds.Height - 40));

            _txtStatus = new TextBox();
            AddChild(_txtStatus, new RectangleF(40, 0, 300, 300));

            _messageWindow = new MessageWindow(500,300);
            _messageWindow.Dock = DockPosition.Top;
            AddChild(_messageWindow, new Vector2(400, 10));

            //_selectionInfoView = new SelectionInfoView();
            //AddChild(_selectionInfoView, new Vector2(Bounds.Width - _selectionInfoView.Bounds.Width, Bounds.Height - _selectionInfoView.Bounds.Height));

            _dockView = new ShipDockView();
            _shipView = new ShipDialog();

            _selectionOverlay = new TextBox();
            _selectionOverlay.Size = new SizeF(400, 80);
            _selectionOverlay.Visible = false;
            _selectionOverlay.AutoSize = true;
            AddChild(_selectionOverlay);
            _hud = this;

            _txtPower = new TextBox();
            _txtPower.AutoSize = true;
            _txtPower.TextColor = Color.White;
            _txtPower.TextScale = 0.6f;
            AddChild(_txtPower, new RectangleF(5, 5, 100, 30));

            _txtPopulation = new TextBox();
            AddChild(_txtPopulation, new RectangleF(5, 40, 100, 30));
            _txtPopulation.TextColor = Color.White;
            _txtPopulation.TextScale = 0.6f;

            _txtOre = new TextBox();
            AddChild(_txtOre, new RectangleF(5, 55, 100, 30));
            _txtOre.TextColor = Color.White;
            _txtOre.TextScale = 0.6f;

            _txtResupply = new TextBox();
            AddChild(_txtResupply, new RectangleF(300, 5, 150, 30));
            _txtResupply.TextColor = Color.White;
            _txtResupply.TextScale = 0.6f;
            _txtResupply.Visible = false;
        }

        public TextBox SelectionOverlay
        {
            get
            {
                return _selectionOverlay;
            }
        }

        public void ShowShipView()
        {
            _shipView.Initialize(_playerInfo);
            _engine.UserInterface.ShowDialog(_shipView);
        }

        public void ShowDockView(Ship ship)
        {
            _dockView.SetShip(ship, _playerInfo);
            _engine.UserInterface.ShowDialog(_dockView);
        }

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            switch (key)
            {
                case Keys.Enter:
                    if (_playerInfo.Ship.IsDocked)
                        ShowDockView(_playerInfo.Ship.DockedWith);
                    else
                        ShowShipView();
                    return KeyPressResult.HandledLatched;
            }
            
            if (key == Keys.C)
            {
                //center camera;
                var selection = _engine.Targetter.CurrentTarget;
                if (selection != null)
                {
                    _engine.CameraTarget = selection;
                    return KeyPressResult.HandledLatched;
                }
            }
            return base.ProcessKey(manager, key);
        }

        public SelectionInfoView SelectionInfoView
        {
            get
            {
                return _selectionInfoView;
            }
        }

        public MessageWindow MessageWindow
        {
            get
            {
                return _messageWindow;
            }
        }

        static Hud _hud;
        public static Hud Instance
        {
            get
            {
                return _hud;
            }
        }

        public void Update(int frameNum)
        {
            _frameNum = frameNum;

            _messageWindow.Update();
            //string status = "$" + Convert.ToString(_playerInfo.Money) + Environment.NewLine;
            //status += "Ship fuel: " + _playerInfo.Ship.Fuel + Environment.NewLine;
            //status += "Ship power: " + _playerInfo.Ship.Power + Environment.NewLine;
            //_txtStatus.Text = status;

            string powerText = "Power generated: " + Convert.ToString(_engine.Colony.PowerGenerated) + Environment.NewLine;
            powerText += "Power Required: " + Convert.ToString(_engine.Colony.PowerRequired) + Environment.NewLine;
            powerText += "Production Efficiency: " + Convert.ToString(_engine.Colony.ProductionEfficiency * 100) + "%";
            _txtPower.Text = powerText;

            _txtPopulation.Text = "Population: " + Convert.ToString(_engine.Colony.Population) + @"/" + Convert.ToString(_engine.Colony.MaxPopulation);
            _txtOre.Text = "Ore: " + Convert.ToString(_engine.Colony.Ore);

            _txtResupply.Text = "Next resupply: 3 days";
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            
        }

        int _frameNum;
        public int FrameNumber 
        {
            get
            {
                return _frameNum;
            }
        }

        internal void UpdateSelectionOverlay(XnaRenderer renderer, Camera2D worldCamera, Engine engine, ITargetable target)
        {
            //convert the world location of the target to screen location on the UI
            Vector2 viewPosition = worldCamera.WorldToView(target.Location);
            Vector2 uiPosition = engine.UserInterface.Camera.ViewToWorld(viewPosition);

            Vector2 screenSize = new Vector2(800, 600);
            Vector2 start = screenSize * 0.5f;//new Vector2(0,0); //renderer.CenterScreen;
            Vector2 end = uiPosition;
            float border = 10f;
            var intersect = FarseerPhysics.Common.LineTools.LineSegmentAABBIntersect(ref start, ref end, new FarseerPhysics.Collision.AABB(new Vector2(border, border), new Vector2(screenSize.X - border * 2f, screenSize.Y - border * 2f)));
            //var intersect = FarseerPhysics.Common.LineTools.LineSegmentAABBIntersect(ref start, ref end, new FarseerPhysics.Collision.AABB(new Vector2(-1f + border, -1f + border), new Vector2(1f - border, 1f - border)));
            if (intersect.Count > 0)
            {
                Vector2 point = intersect[0];
                uiPosition = point;
                float rotation = LiteEngine.Math.Util.AngleBetween(start, uiPosition);

                renderer.DrawSprite(new LiteEngine.Textures.Texture("triangleOverlay"), uiPosition, new Vector2(10, 15), rotation, Color.White, 1f);
                float ratioX = uiPosition.X / _engine.ScreenSize.Width;
                float ratioY = uiPosition.Y / _engine.ScreenSize.Height;
                uiPosition.X = uiPosition.X - ratioX * (SelectionOverlay.Bounds.Width + 16) + 8;
                uiPosition.Y = uiPosition.Y - ratioY * (SelectionOverlay.Bounds.Height + 16) + 8;
                SelectionOverlay.Position = uiPosition; // new Vector2(uiPos.X - SelectionOverlay.Bounds.Width / 2, uiPos.Y + 10);
            }
            else
                SelectionOverlay.Position = uiPosition;

            int distance = (int)(_playerInfo.Ship.Position - target.Location).Length();
            string text = target.Info.Name;
            text += Environment.NewLine + "[" + target.Info.Type + "]";
            text += Environment.NewLine + "dist: " + distance + "m";
            _selectionOverlay.Text = text;
            SelectionOverlay.Visible = true;
        }
    }

    class PowerBar : BaseUIControl
    {
        PlayerInfo _playerInfo;
        public PowerBar(PlayerInfo playerInfo)
        {
            _playerInfo = playerInfo;
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            float fractionPower = (float)_playerInfo.Ship.Power / 30;
            float powerHeight = Bounds.Height * fractionPower;
            renderer.DrawSprite(new Texture("dialog"), new RectangleF(0,0,Bounds.Width, Bounds.Height));
            renderer.DrawSprite(new Texture("dialog"), new RectangleF(2, Bounds.Height - powerHeight, Bounds.Width - 4, powerHeight), Color.Cyan);
        }
    }
}
