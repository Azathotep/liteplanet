﻿using LiteEngine.Math;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class StoreItemPreviewBox : Dialog
    {
        ImageControl _icon;
        TextBox _itemTitle;
        TextBox _itemSummary;
        TextBox _itemDescription;
        public StoreItemPreviewBox()
        {
            Size = new SizeF(300, 500);
            _icon = new ImageControl();
            AddChild(_icon, new RectangleF(5, 5, 60, 60));
            _itemTitle = new TextBox();
            AddChild(_itemTitle, new RectangleF(_icon.Bounds.Right + 5, 5, Bounds.Width - _icon.Bounds.Width - 15, 15));
            _itemSummary = new TextBox();
            AddChild(_itemSummary, new RectangleF(_icon.Bounds.Right + 5, 25, Bounds.Width - _icon.Bounds.Width - 15, 50));
            _itemDescription = new TextBox();
            AddChild(_itemDescription, new RectangleF(15, 70, Bounds.Width - 30, 300));
        }

        public void SetItem(ShipComponent component)
        {
            _icon.Image = component.Image;
            _itemTitle.Text = component.Name;
            _itemSummary.Text = component.ShortDescription;

            _itemDescription.Text = "Description" + Environment.NewLine + Environment.NewLine;
            _itemDescription.Text += "Press [enter] to purchase for $" + component.BaseCost;
        }
    }
}
