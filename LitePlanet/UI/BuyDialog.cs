﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class BuyDialog : Dialog
    {
        GridControl _grid;
        StoreItemPreviewBox _itemPreview;
        PlayerInfo _player;
        public BuyDialog(PlayerInfo player, OnDialogCompleteHandler onComplete) : base(onComplete)
        {
            _player = player;
            Size = new SizeF(700, 600);
            _grid = new GridControl(6, 10, 30, 40);
            
            TextBox title = new TextBox();
            title.Text = "Welcome to the Station 109 Store" + Environment.NewLine;
            title.Text += "Available products are listed below" + Environment.NewLine;
            AddChild(title, new RectangleF(15, 15, Bounds.Width - 30, 50));

            TextBox exitText = new TextBox();
            exitText.Text = "[Press Escape to Exit Store]";
            AddChild(exitText, new RectangleF(400, 15, 300, 15));

            

            RectangleF gridBounds = new RectangleF(10, 70, 300, 500);
            AddChild(_grid, gridBounds.Grow(-10));

            for (int i = 0; i < 6; i++)
            {
                _grid.AddControl(new ShopItemBox(new Blaster()), 0, i);
                _grid.AddControl(new ShopItemBox(new BlasterII()), 1, i);
                _grid.AddControl(new ShopItemBox(new SmokerEngine()), 2, i);
                _grid.AddControl(new ShopItemBox(new FuelReactor()), 3, i);
            }


            _itemPreview = new StoreItemPreviewBox();
            AddChild(_itemPreview, new RectangleF(400, 60, 400, 400));

            BackgroundColor = Color.FromNonPremultiplied(200, 200, 200, 255);
        }

        void AddItem(ShipComponent part, RectangleF position)
        {
            ShopItemBox item = new ShopItemBox(part);
            AddChild(item, position);
        }

        Vector2I _cursorPosition;
        int _frame;
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            _frame++;
            if (_frame % 30 < 15)
                SelectedItem.BorderWidth = 3;
            else
                SelectedItem.BorderWidth = 0;
        }

        ShopItemBox SelectedItem
        {
            get
            {
                return _grid.GetCellItem(_cursorPosition.X, _cursorPosition.Y) as ShopItemBox;
            }
        }

        ShipComponent _purchasedItem;
        public ShipComponent PurchasedItem
        {
            get
            {
                return _purchasedItem;
            }
        }

        void BuySelectedItem(UserInterface ui)
        {
            var item = SelectedItem.Component;
            if (_player.Money < item.BaseCost)
            {
                string msg = "Sorry you need ${0} to buy this {1}";
                msg = String.Format(msg, item.BaseCost, item.Name);
                ui.ShowDialog(new MessageDialog(msg));
                return;
            }

            string message = "Purchase and install " + item.Name + " for $" + item.BaseCost + "?";
            ConfirmationDialog dialog = new ConfirmationDialog(message, (d) =>
                {
                    if (d.YesResult)
                    {
                        _purchasedItem = SelectedItem.Component;
                        _player.Money -= _purchasedItem.BaseCost;
                        Close();
                    }
                });
            ui.ShowDialog(dialog);
        }

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            switch (key)
            {
                case Keys.Escape:
                    Close();
                    return KeyPressResult.HandledLatched;
                case Keys.Enter:
                case Keys.B:
                    BuySelectedItem(manager);
                    return KeyPressResult.HandledLatched;
            }

            if (key == Keys.Right || key == Keys.Left || key == Keys.Up || key == Keys.Down)
            {
                SelectedItem.BorderWidth = 0;
                Vector2I oldCursorPosition = _cursorPosition;
                switch (key)
                {
                    case Keys.Up:
                        _cursorPosition.Y--;
                        break;
                    case Keys.Down:
                        _cursorPosition.Y++;
                        break;
                    case Keys.Right:
                        _cursorPosition.X++;
                        break;
                    case Keys.Left:
                        _cursorPosition.X--;
                        break;
                }
                if (_grid.GetCellItem(_cursorPosition.X, _cursorPosition.Y) == null)
                    _cursorPosition = oldCursorPosition;
                _itemPreview.SetItem(SelectedItem.Component);
                _frame = 0;
            }
            return base.ProcessKey(manager, key);
        }
    }

    class GridControl : BaseUIControl
    {
        BaseUIControl[,] _grid;
        Vector2I _size;
        int _cellWidth;
        int _cellHeight;
        public GridControl(int rows, int columns, int cellWidth, int cellHeight)
        {
            _size = new Vector2I(rows, columns);
            _grid = new BaseUIControl[rows, columns];
            _cellWidth = cellWidth;
            _cellHeight = cellHeight;
        }

        public void AddControl(BaseUIControl control, int x, int y)
        {
            float borderSize = 10;
            _grid[x, y] = control;
            float controlWidth = (Bounds.Width - borderSize * (_size.X + 1)) / _size.X;
            float controlHeight = (Bounds.Height - borderSize * (_size.Y + 1)) / _size.Y;
            AddChild(control, new RectangleF(x * (borderSize + controlWidth), y * (borderSize + controlHeight), controlWidth, controlHeight));
        }
    
        internal BaseUIControl GetCellItem(int x, int y)
        {
 	        if (x < 0 || y < 0)
                return null;
            if (x >= Bounds.Width || y >= Bounds.Height)
                return null;
            return _grid[x,y];
        }
    }

    class ShopItemBox : BaseUIControl
    {
        ShipComponent _part;
        public ShopItemBox(ShipComponent part)
        {
            _part = part;
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            renderer.DrawSprite(_part.Image, Bounds);
            renderer.DrawStringBox(Convert.ToString(_part.BaseCost), new RectangleF(Bounds.Left, Bounds.Top + Bounds.Height - 15, Bounds.Width, 15), Color.Red);
        }

        public ShipComponent Component
        {
            get
            {
                return _part;
            }
        }
    }
}
