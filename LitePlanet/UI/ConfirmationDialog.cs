﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class ConfirmationDialog : Dialog
    {
        OnDialogConfirmationHandler _onConfirmation;
        public ConfirmationDialog(string message, OnDialogConfirmationHandler onConfirmation=null)
        {
            _onConfirmation = onConfirmation;
            Size = new SizeF(350, 100);

            TextBox text = new TextBox();
            text.Text = message + Environment.NewLine;
            text.Text += "[enter] to confirm   [escape] to cancel";
            AddChild(text, Bounds.Grow(-10));
            BackgroundColor = Color.SlateGray;
        }

        public override void Close()
        {
            base.Close();
            if (_onConfirmation != null)
                _onConfirmation(this);
        }

        public bool YesResult = false;

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            switch (key)
            {
                case Keys.Escape:
                case Keys.N:
                    Close();
                    return KeyPressResult.HandledLatched;
                case Keys.Enter:
                case Keys.Y:
                    YesResult = true;
                    Close();
                    return KeyPressResult.HandledLatched;
            }
            return base.ProcessKey(manager, key);
        }
    }
    delegate void OnDialogConfirmationHandler(ConfirmationDialog dialog);
}
