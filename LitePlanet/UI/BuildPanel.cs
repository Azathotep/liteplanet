﻿using LiteEngine.Math;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using LitePlanet.Structures;
using LitePlanet.Vessels;
using LitePlanet.Worlds;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class BuildPanel : BaseUIControl
    {
        Engine _engine;
        ImageControl _iconBox;
        TextBox _txtDesc;
        public BuildPanel(Engine engine)
        {
            _engine = engine;
            BackgroundColor = Color.LightGray;

            AddButton(StructureType.SolarPanel, new RectangleF(5, 5, 20, 20));
            AddButton(StructureType.DrillTop, new RectangleF(35, 5, 20, 20));
            AddButton(StructureType.Housing, new RectangleF(65, 5, 20, 20));
            AddButton(StructureType.Mine, new RectangleF(5, 35, 20, 20));
            AddButton(StructureType.Turret, new RectangleF(35, 35, 20, 20));

            _iconBox = new ImageControl();
            _iconBox.BorderWidth = 1f;
            AddChild(_iconBox, new RectangleF(5, 70, 40, 40));

            _txtDesc = new TextBox();
            _txtDesc.TextScale = 0.5f;
            AddChild(_txtDesc, new RectangleF(5, 115, 90, 70));
        }

        void AddButton(StructureType structure, RectangleF bounds)
        {
            StructureButton button = new StructureButton(structure);
            button.OnClick = ButtonClick;
            AddChild(button, bounds);
        }

        void ButtonClick(Button sender)
        {
            StructureType structure = ((StructureButton)sender).Structure;

            _txtDesc.Text = "==" + StructureFactory.GetName(structure) + "==" + Environment.NewLine +
                StructureFactory.GetDescription(structure) + Environment.NewLine + Environment.NewLine +
                "Cost: " + StructureFactory.GetOreCost(structure) + " Ore";

            _engine.SelectedBuildStructureType = structure;
            sender.BorderWidth = 2f;
            _iconBox.Image = StructureFactory.GetIcon(structure);
        }
    }

    class StructureButton : Button
    {
        StructureType _structure;
        public StructureButton(StructureType structure)
        {
            _structure = structure;
            ImageControl imageControl = new ImageControl();
            imageControl.Image = StructureFactory.GetIcon(structure);
            imageControl.Size = new SizeF(15, 15);
            imageControl.Dock = DockPosition.Center;
            AddChild(imageControl);
        }

        public StructureType Structure
        {
            get
            {
                return _structure;
            }
        }
    }
}
