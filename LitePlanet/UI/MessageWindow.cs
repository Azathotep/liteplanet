﻿using LiteEngine.Math;
using LiteEngine.UI;
using LitePlanet.Comms;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class MessageWindow : StackPanel
    {
        List<Message> _messages = new List<Message>();
        MessageTextField[] _textBoxes;
        public MessageWindow(int width, int height)
        {
            Size = new SizeF(width, height);
            _textBoxes = new MessageTextField[5];
            for (int i = 0; i < 5; i++)
            {
                _textBoxes[i] = new MessageTextField();
                AddChild(_textBoxes[i]);
            }
        }

        class MessageTextField : TextBox
        {
            Message _message;
            internal void SetMessage(Message message)
            {
                _message = message;
                TextColor = Color.White;
            }

            public Message Message
            {
                get
                {
                    return _message;
                }
            }

            public void Update()
            {
                if (_message == null)
                    return;

                _message.Age++;
                if (_message.Type == MessageType.Communication)
                {
                    //int numCharsToDraw = Math.Min(_message.Age / 2, _message.Text.Length);
                    Text = _message.Sender.Info.Name + " [" + _message.Sender.Info.Type + "]: " + _message.Text; //.Substring(0, numCharsToDraw);
                }
                else
                {
                    Text = _message.Text;
                }

                if (_message.Age > 500)
                {
                    float fadeAmount = (_message.Age - 500f) / 200f;
                    TextColor = Color.Lerp(Color.White, Color.Transparent, fadeAmount);
                    if (fadeAmount > 1f)
                        Clear();
                }
            }

            public bool IsEmpty
            {
                get
                {
                    return _message == null;
                }
            }

            public void Clear()
            {
                _message = null;
                Text = "";
            }
        }

        Queue<Message> _addQueue = new Queue<Message>();
        int _messageAddDelay = 0;
        public void AddMessage(Message message)
        {
            _addQueue.Enqueue(message);
        }

        void ProcessMessageQueue()
        {
            if (_messageAddDelay > 0)
                _messageAddDelay--;
            else
            {
                if (_addQueue.Count > 0)
                {
                    for (int i = 0; i < _textBoxes.Length; i++)
                        if (_textBoxes[i].IsEmpty)
                        {
                            Message message = _addQueue.Dequeue();
                            _textBoxes[i].SetMessage(message);
                            _messageAddDelay = 10;
                            break;
                        }
                }
            }
        }

        public void Update()
        {
            ProcessMessageQueue();

            for (int i = 0; i < _textBoxes.Length; i++)
                _textBoxes[i].Update();

            //shift messages up into empty slots
            for (int i = 0; i < _textBoxes.Length - 1; i++)
            {
                if (_textBoxes[i].IsEmpty && !_textBoxes[i + 1].IsEmpty)
                {
                    _textBoxes[i].SetMessage(_textBoxes[i + 1].Message);
                    _textBoxes[i + 1].Clear();
                }
            }

        }
    }
}
