﻿using LiteEngine.Rendering;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using LiteEngine.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiteEngine.Math;
using Microsoft.Xna.Framework;

namespace LitePlanet.UI
{
    class ShipComponentImageBox : BaseUIControl
    {
        ShipGridCell _cell;
        public ShipComponentImageBox(ShipGridCell cell)
        {
            _cell = cell;
        }

        public ShipGridCell Cell
        {
            get
            {
                return _cell;
            }
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture = new Texture("shipparts", new RectangleI(0, 0, 32, 32));
            renderer.DrawSprite(texture, Bounds, Color.SlateGray);
            if (Cell.Component != null)
            {
                renderer.DrawSprite(Cell.Component.Image, Bounds, 0);
            }
        }

        void DrawBorder(XnaRenderer renderer, Texture texture, int size)
        {
            RectangleF border = Bounds.Grow(size);
            renderer.DrawSprite(texture, border, Color.Green);
        }
    }
}
