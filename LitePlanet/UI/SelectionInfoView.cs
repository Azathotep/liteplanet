﻿using LiteEngine.Math;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using LitePlanet.Vessels;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class SelectionInfoView : Dialog
    {
        ITargetable _selection;
        ImageControl _icon;
        TextBox _txtName;
        TextBox _txtCommands;
        public SelectionInfoView()
        {
            Size = new SizeF(300, 200);
            _icon = new ImageControl();
            _icon.BorderWidth = 3;
            _txtName = new TextBox();
            _txtName.TextColor = Color.Black;
            _txtCommands = new TextBox();
            _txtCommands.TextColor = Color.Black;

            AddChild(_icon, new RectangleF(5, 5, 60, 60));
            AddChild(_txtName, new RectangleF(70, 5, 200, 20));
            AddChild(_txtCommands, new RectangleF(5, 70, Bounds.Width - 10, Bounds.Height - 70));
        }

        public ITargetable Selection
        {
            get
            {
                return _selection;
            }
        }

        public void SetSelection(ITargetable selection)
        {
            _selection = selection;
            _txtName.Text = "Name: " + _selection.Info.Name;
            _txtName.Text += Environment.NewLine + "Type: " + _selection.Info.Type;
            //_txtName.Text += Environment.NewLine + "Alignment: " + "Civilian";

            _txtCommands.Text = "Commands:";
            _txtCommands.Text += Environment.NewLine + "[i] more info";
            _txtCommands.Text += Environment.NewLine + "[c] center camera";
        }
    }
}
