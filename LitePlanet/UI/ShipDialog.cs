﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class ShipDialog : Dialog
    {
        ComponentStatusPane _componentDialog;
        ShipComponentImageBox[,] _imageGrid;
        PlayerInfo _player;
        public ShipDialog()
        {
            _componentDialog = new ComponentStatusPane();
            AddChild(_componentDialog, new Vector2(300, 0));

            //TextControl t = new TextControl();
            //t.Text = "R - Repair Selected Component" + Environment.NewLine +
            //         "U - Upgrade Selected Component" + Environment.NewLine +
            //         "S - Sell Selected Component" + Environment.NewLine +
            //         "C - View Cargo";

            //t.Background = new LiteEngine.Textures.Texture("dialog");
            //AddChild(t, new RectangleF(200, 0, 300, 100));

            //TextControl tt = new TextControl();
            //tt.Text = "Engine" + Environment.NewLine +
            //          "Status: Damaged";
            //AddChild(tt, new RectangleF(80, 20, 200, 100));
        }

        public void Initialize(PlayerInfo player)
        {
            _player = player;
            ShipLayout layout = player.Ship.Layout;
            if (_imageGrid == null)
            {
                _imageGrid = new ShipComponentImageBox[layout.Width, layout.Height];
                Size = new SizeF(600, 300);
                for (int y = 0; y < layout.Height; y++)
                    for (int x = 0; x < layout.Width; x++)
                    {
                        if (layout.Grid[x, y] != null)
                            _imageGrid[x, y] = AddComponentBox(65 + x * 60, 40 + y * 60, layout.Grid[x, y]);
                    }
                _selectedComponent.X = layout.Width / 2 + 1;
                _selectedComponent.Y = layout.Height / 2 + 1;
            }
            SelectionChanged();
        }

        ShipComponentImageBox AddComponentBox(float x, float y, ShipGridCell cell)
        {
            ShipComponentImageBox image = new ShipComponentImageBox(cell);
            AddChild(image, new RectangleF(x, y, 50, 50));
            return image;
        }

        void ShowStore(UserInterface ui)
        {
            BuyDialog dialog = new BuyDialog(_player, onBuyDialogComplete);
            ui.ShowDialog(dialog);
        }

        void onBuyDialogComplete(Dialog dialog)
        {
            BuyDialog buyDialog = dialog as BuyDialog;
            ShipComponent purchase = buyDialog.PurchasedItem;
            if (purchase != null)
            {
                _player.Ship.Layout.InstallComponent(_selectedComponent.X, _selectedComponent.Y, purchase);
                SelectionChanged();
            }
        }

        void SellSelectedPart(UserInterface ui)
        {
            ShipComponent part = SelectedComponentImage.Cell.Component;
            if (part == null)
                return;
            string message = String.Format("Uninstall and sell selected part ({0}) for ${1}?", part.Name, part.BaseCost);

            ConfirmationDialog dialog = new ConfirmationDialog(message,
                (d) =>
                {
                    if (d.YesResult)
                    {
                        _player.Money += part.BaseCost;
                        SelectedComponentImage.Cell.RemoveComponent();
                        SelectionChanged();
                    }
                
                });
            ui.ShowDialog(dialog);
        }

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            switch (key)
            {
                case Keys.Escape:
                    Close();
                    return KeyPressResult.HandledLatched;
                case Keys.B:
                case Keys.Enter:
                    if (SelectedComponentImage.Cell.Component == null)
                        ShowStore(manager);
                    return KeyPressResult.HandledLatched;
                case Keys.S:
                    SellSelectedPart(manager);
                    return KeyPressResult.HandledLatched;
            }           


            if (key == Keys.Right || key == Keys.Left || key == Keys.Up || key == Keys.Down)
            {
                SelectedComponentImage.BorderWidth = 0;
                Vector2I oldSelected = _selectedComponent;
                switch (key)
                {
                    case Keys.Up:
                        _selectedComponent.Y--;
                        break;
                    case Keys.Down:
                        _selectedComponent.Y++;
                        break;
                    case Keys.Right:
                        _selectedComponent.X++;
                        break;
                    case Keys.Left:
                        _selectedComponent.X--;
                        break;
                }
                if (_selectedComponent.X >= _player.Ship.Layout.Width || _selectedComponent.Y >= _player.Ship.Layout.Height ||
                    _selectedComponent.X < 0 || _selectedComponent.Y < 0)
                    _selectedComponent = oldSelected;
                if (SelectedComponentImage == null)
                    _selectedComponent = oldSelected;

                SelectionChanged();
                frame = 0;
            }
            return base.ProcessKey(manager, key);
        }

        ShipComponentImageBox SelectedComponentImage
        {
            get
            {
                return _imageGrid[_selectedComponent.X, _selectedComponent.Y];
            }
        }

        void SelectionChanged()
        {
            _componentDialog.SetCell(SelectedComponentImage.Cell);
        }

        Vector2I _selectedComponent;

        int frame;
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            renderer.DrawSprite(new LiteEngine.Textures.Texture("shipmenu"), new RectangleF(Bounds.Left,Bounds.Top,Bounds.Width/2, Bounds.Height));

            var selectedImage = _imageGrid[_selectedComponent.X, _selectedComponent.Y];
            if (frame % 30 < 15)
                selectedImage.BorderWidth = 3;
            else
                selectedImage.BorderWidth = 0;
            frame++;
        }
    }
}
