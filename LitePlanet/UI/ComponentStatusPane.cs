﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class ComponentStatusPane : BaseUIControl
    {
        TextBox _title;
        TextBox _description;
        TextBox _status;
        ImageControl _icon;
        
        public ComponentStatusPane()
        {
            Size = new SizeF(300, 300);
            _icon = new ImageControl();
            _icon.BorderWidth = 3;
            AddChild(_icon, new RectangleF(5, 5, 60, 60));
            _title = new TextBox();
            _title.Text = "";
            AddChild(_title, new RectangleF(_icon.Bounds.Right + 10, 5, Bounds.Width - _icon.Bounds.Width - 15, 15));

            _description = new TextBox();
            AddChild(_description, new RectangleF(_icon.Bounds.Right + 10, 25, Bounds.Width - _icon.Bounds.Width - 15, 40));

            _status = new TextBox();
            AddChild(_status, new RectangleF(10, _icon.Bounds.Bottom + 5, Bounds.Width - 10, 200));
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Color c = Color.FromNonPremultiplied(255, 255, 255, 160);
            renderer.DrawSprite(new Texture("dialog"), Bounds, c);
        }

        public void SetCell(ShipGridCell cell) 
        {
            ShipComponent part = cell.Component;
            if (part == null)
            {
                _title.Text = "Empty slot";
                _description.Text = "This component slot is empty";
                _icon.Image = null;
                _status.Text = "";
                _status.Text += "Available Commands:" + Environment.NewLine;
                _status.Text += "[b] buy and install new part";
                return;
            }

            _title.Text = part.Name;
            _icon.Image = part.Image;
            _description.Text = part.ShortDescription;
            _status.Text = "";
            _status.Text += "Integrity: " + part.Health + "/" + part.MaxHealth + Environment.NewLine;

            foreach (string status in part.GenerateStatusText())
            {
                _status.Text += status + Environment.NewLine;
            }
            _status.Text += "" + Environment.NewLine;
            _status.Text += "Available Commands:" + Environment.NewLine;
            if (part.Health < part.MaxHealth)
                _status.Text += "[r] repair part for $50";

            _status.Text += "[s] sell part for $" + part.BaseCost + Environment.NewLine;
            _status.Text += "[u] upgrade part" + Environment.NewLine;
        }
    }
}
