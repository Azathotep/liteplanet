﻿using LiteEngine.Math;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using LitePlanet.Vessels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class ShipDockView : Dialog
    {
        PlayerInfo _player;
        Ship _ship;
        TextBox _txtWelcome;
        TextBox _txtCommands;
        public ShipDockView()
        {
            Size = new SizeF(500, 400);
            _txtWelcome = new TextBox();
            _txtWelcome.TextColor = Color.Black;
            _txtCommands = new TextBox();
            _txtCommands.TextColor = Color.Black;

            StackPanel stack = new StackPanel();
            AddChild(stack, new RectangleF(10, 10, Bounds.Width - 20, Bounds.Height - 20));
            stack.AddChild(_txtWelcome);
            stack.AddChild(_txtCommands);
            BackgroundColor = Color.FromNonPremultiplied(new Vector4(1f, 1f, 1f, 0.95f));
        }

        public void SetShip(Ship ship, PlayerInfo player)
        {
            _ship = ship;
            _player = player;
            _txtWelcome.Text = "Welcome aboard the " + ship.Info.Name;
            _txtWelcome.Text += Environment.NewLine + "We supply fuel, repairs and ship parts at great prices";
            _txtWelcome.Text += Environment.NewLine;
            UpdateDisplay();
        }

        void UpdateDisplay()
        {
            int fuelCanBuy = _player.Money / 2;
            fuelCanBuy = Math.Min(fuelCanBuy, 1000 - _player.Ship.Fuel);
            fuelCanBuy = Math.Min(fuelCanBuy, 100);
            int fuelCost = (int)fuelCanBuy * 2;
            
            _txtCommands.Text = "Available Options:" + Environment.NewLine;

            if (fuelCanBuy > 0)
                _txtCommands.Text += Environment.NewLine + "[f]     Buy " + fuelCanBuy + " fuel for ($" + fuelCost + ")";
            else
                _txtCommands.Text += Environment.NewLine + "[Your ship is fully fueled]";
            _txtCommands.Text += Environment.NewLine + "[enter] Buy/Sell/Repair ship parts";
            _txtCommands.Text += Environment.NewLine + "[i]     View information about the " + _ship.Info.Name;
            _txtCommands.Text += Environment.NewLine + "[esc]   Exit this terminal (view space or launch ship)";
        }

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            switch (key)
            {
                case Keys.Enter:
                    Hud.Instance.ShowShipView();
                    return KeyPressResult.HandledLatched;
                case Keys.Escape:
                    Close();
                    return KeyPressResult.HandledLatched;
                case Keys.F:
                    int fuelCanBuy = _player.Money / 2;
                    fuelCanBuy = Math.Min(fuelCanBuy, 1000 - _player.Ship.Fuel);
                    fuelCanBuy = Math.Min(fuelCanBuy, 100);
                    int fuelCost = (int)fuelCanBuy * 2;
                    _player.Ship.Fuel += fuelCanBuy;
                    _player.Money -= fuelCost;
                    UpdateDisplay();
                    return KeyPressResult.HandledLatched;
            }
            return base.ProcessKey(manager, key);
        }
    }

    interface IStore
    {
        float CostModifier
        {
            get;
        }

        List<ShipComponent> Parts
        {
            get;
        }
    }
}
