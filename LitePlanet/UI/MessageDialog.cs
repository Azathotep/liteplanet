﻿using LiteEngine.Math;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.UI;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.UI
{
    class MessageDialog : Dialog
    {
        public MessageDialog(string message)
        {
            Size = new SizeF(350, 100);

            TextBox text = new TextBox();
            text.Text = message + Environment.NewLine;
            text.Text += "[Press any key to continue]";
            text.TextColor = Color.White;
            AddChild(text, Bounds.Grow(-10));
            BackgroundColor = Color.SlateGray;
        }

        public override KeyPressResult ProcessKey(UserInterface manager, Keys key)
        {
            Close();
            return KeyPressResult.HandledLatched;
        }
    }
}
