﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using LiteEngine.Particles;
using LiteEngine.Physics;
using LitePlanet.Effects;
using LiteEngine.Core;
using LiteEngine.Rendering;
using LitePlanet.Worlds;

namespace LitePlanet.Projectiles
{
    internal class Shells
    {
        Engine _engine;
        ParticlePool _pool;
        public Shells(Engine engine)
        {
            _engine = engine;
            _pool = engine.ParticleSystem.CreateParticleFactory();
        }

        public void UpdateCollisionFields(PlanetCollisionField collisionField)
        {
            foreach (Particle p in _pool.Particles)
            {
                collisionField.CreateCollisionTilesAroundPoint(p.Position);
            }
        }

        public Particle CreateShell(Vector2 position, Vector2 velocity)
        {
            Particle particle = _pool.CreateParticle(position, velocity, 2000);
            particle.Body.IsBullet = true;
            particle.Body.CollidesWith = Category.Cat1 | Category.Cat2;
            particle.Body.CollisionCategories = Category.Cat2;
            particle.Body.Enabled = false;
            particle.Body.Enabled = true;
            particle.Body.Mass = 10000000f;
            particle.Body.IgnoreGravity = false;
            particle.OnCollideWithOther += particle_OnCollideWithOther;
            return particle;
        }

        void particle_OnCollideWithOther(Particle particle, IPhysicsObject other, float impulse)
        {
            particle.Life = 0;
            Explosion explosion = new Explosion(_engine);
            explosion.Create(particle.Position);
            IDamageSink damageSink = other as IDamageSink;
            if (damageSink != null)
                damageSink.TakeDamage(20);
        }

        internal void Draw(XnaRenderer renderer)
        {
            foreach (Particle p in _pool.Particles)
            {
                renderer.DrawPoint(p.Position, 1f, Color.White, 1f);
            }
        }
    }
}
