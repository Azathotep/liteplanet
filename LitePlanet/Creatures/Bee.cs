﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Physics;
using LiteEngine.Textures;
using LitePlanet.Vessels;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Creatures
{
    class Bee : IPhysicsObject
    {
        Body _body;

        public Bee(PhysicsCore physics)
        {
            _body = physics.CreateBody(this);
            _body.Enabled = false;
            _body.BodyType = BodyType.Dynamic;
            _body.AngularDamping = 0.5f;
            _body.Friction = 1f;
            _body.Restitution = 1.1f;
            _body.Mass = 0.5f;
            _body.Rotation = 0f;
            FixtureFactory.AttachCircle(0.1f, 1f, _body);
            _body.CollisionCategories = Category.Cat2;
            _body.CollidesWith = Category.Cat1 | Category.Cat2 | Category.Cat3;
            _body.Enabled = true;
        }

        public void Think()
        {
            if (_target != null)
            {
                Vector2 idealVelocity = _target.Position - _body.Position;
                idealVelocity.Normalize();
                idealVelocity *= 20f;

                Vector2 velocity = _body.LinearVelocity;
                Vector2 velChange = idealVelocity - velocity;
                if (float.IsNaN(velChange.X))
                {
                    int a = 1;
                }
                _body.ApplyForce(velChange * 0.05f);
            }
            else
                _body.ApplyForce(Dice.RandomVector2(0.1f));
        }

        public Body Body
        {
            get { return _body; }
        }

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {

        }

        Ship _target;
        internal void SetTarget(Ship target)
        {
            _target = target;
        }

        internal void Draw(LiteEngine.Rendering.XnaRenderer renderer)
        {
            Texture texture = TextureBook.GetTexture(@"textures\planet1.bee");

            float facing = _body.Rotation;
            if (_target != null)
            {
                //facing = Util.AngleBetween(_body.Position, _target.Position);
            }
            facing = Util.AngleBetween(_body.Position, _body.Position + _body.LinearVelocity);
            renderer.DrawSprite(texture, Body.Position, new Vector2(1,1), facing);

            renderer.DrawDepth -= 0.1f;
            Texture wings = TextureBook.GetTexture(@"textures\planet1.beewing");
            float wingSize = 1.2f + Dice.RandomFloat(0.3f);
            renderer.DrawSprite(wings, Body.Position, new Vector2(wingSize, 0.8f), facing, 0.4f + Dice.RandomFloat(0.3f));

            renderer.DrawDepth += 0.1f;
        }
    }
}
