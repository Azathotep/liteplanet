﻿using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Rendering;
using LitePlanet.Structures;
using LitePlanet.UI;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    public class Colony
    {
        public int Population = 20;
        public int MaxPopulation = 20;
        public int PowerGenerated;
        public int PowerRequired;
        public int Ore = 150;
        int _numStructures = 0;


        public float ProductionEfficiency
        {
            get
            {
                if (PowerRequired == 0)
                    return 0f;
                return Math.Min(1f, PowerGenerated / (float)PowerRequired);
            }
        }

        /// <summary>
        /// Returns true if the specified structure can be built on the specified tile
        /// </summary>
        internal bool CanBuild(StructureType type, PlanetTile tile)
        {
            if (tile == null)
                return false;
            if (_numStructures == 0)
                return true;
            StructurePlacementValidator validator = StructureFactory.GetPlacementValidator(type);
            bool valid = validator.Validate(tile);
            if (!valid && validator.ErrorMessage.Length > 0)
            {
                Hud.Instance.MessageWindow.AddMessage(new Comms.Message(validator.ErrorMessage, Comms.MessageType.Event));
                //output error message
            }
            return valid;
        }

        internal void Build(StructureType type, PlanetTile tile)
        {
            int oreCost = StructureFactory.GetOreCost(type);
            if (Ore < oreCost)
            {
                Hud.Instance.MessageWindow.AddMessage(new Comms.Message("Insufficient Ore", Comms.MessageType.Event));
                return;
            }

            if (!CanBuild(type, tile))
                return;

            Ore -= oreCost;

            ColonyStructure structure = StructureFactory.CreateStructure(type);
            structure.Colony = this;
            structure.Tile = tile;
            tile.Structure = structure;
            _numStructures++;

            if (structure.GetType() == typeof(Housing))
                MaxPopulation += 20;
        }

        internal void RemoveStructure(ColonyStructure structure)
        {
            structure.Stop();
            structure.Colony = null;
            _numStructures--;

            if (structure.GetType() == typeof(Housing))
                MaxPopulation -= 20;
        }

        internal void GrowPopulation()
        {
            Population++;
        }
    }

    struct PlacementCell
    {
        public bool StructurePart;
        public bool Valid;
        public bool Ground;
    }

    class BuildPlacementLayout
    {
        Vector2I _anchorCell;
        PlacementCell[,] _cells;
        bool[,] _valid;

        public BuildPlacementLayout()
        {
            _cells = new PlacementCell[3, 2];
            _cells[0, 0].StructurePart = true;
            _cells[1, 0].StructurePart = true;
            _cells[2, 0].StructurePart = true;
            _cells[1, 1].StructurePart = true;
            _anchorCell = new Vector2I(1, 0);
        }

        public void Validate(PlanetTile anchorTile)
        {
            //GridHelper.Foreach<bool>(_cells, (x, y) =>
            //    {


            //    });


            //for (int y=0;y<_cells.
            //        _valid[x, y] = false;
            //        PlanetTile tile = anchorTile.GetNeighbour(x - _anchorCell.X, y - _anchorCell.Y);
            //        if (tile == null)
            //            return;
            //        if (tile.IsEmpty)
            //            _valid[x, y] = true;
            //    });
        }

        internal void DrawOverlay(XnaRenderer renderer, PlanetTile anchorTile)
        {
            //GridHelper.Foreach<bool>(_cells, (x, y) =>
            //    {
            //        if (_cells[x, y])
            //        {
            //            PlanetTile tile = anchorTile.GetNeighbour(x - _anchorCell.X, y - _anchorCell.Y);
            //            if (tile == null)
            //                return;
            //            Color color = Color.Red;
            //            if (_valid[x,y])
            //                color = Color.Green;
            //            tile.DrawHighlight(renderer, color);
            //        }
            //    });
        }
    }
}
