﻿using LitePlanet.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    class ColonyRegionManager
    {
        int _nextColonyRegionId = 1;
        Dictionary<int, ColonyRegion> _regions = new Dictionary<int, ColonyRegion>();

        internal ColonyRegion CreateRegion(PlanetTile rootTile)
        {
            ColonyRegion region = new ColonyRegion();
            region.Tile = rootTile;
            region.Id = _nextColonyRegionId;
            _regions[_nextColonyRegionId] = region;
            _nextColonyRegionId++;
            return region;
        }

        public ColonyRegion GetRegion(int regionId)
        {
            ColonyRegion ret = null;
            _regions.TryGetValue(regionId, out ret);
            return ret;
        }

        internal void RemoveRegion(int id)
        {
            _regions.Remove(id);
        }

        internal void CombineRegions(ColonyRegion from, ColonyRegion into)
        {
            //tranfer the tiles of the from region to the into region
            TransferTiles(from, into);
            //transfer properties of from region to into region
            into.GeneratedPower += from.GeneratedPower;
            into.UsedPower += from.UsedPower;
            into.Size += from.Size;
        }

        /// <summary>
        /// Changes all the tiles of a region to belong to a different region
        /// </summary>
        /// <param name="fromRegion"></param>
        /// <param name="newRegionId"></param>
        void TransferTiles(ColonyRegion fromRegion, ColonyRegion toRegion)
        {
            HashSet<PlanetTile> visited = new HashSet<PlanetTile>();
            Queue<PlanetTile> toVisit = new Queue<PlanetTile>();
            toVisit.Enqueue(fromRegion.Tile);
            while (toVisit.Count > 0)
            {
                PlanetTile tile = toVisit.Dequeue();
                tile.ColonyRegionId = toRegion.Id;
                visited.Add(tile);
                foreach (var n in tile.GetAdjacentTiles())
                {
                    if (visited.Contains(n))
                        continue;
                    if (n.ColonyRegionId != fromRegion.Id)
                        continue;
                    toVisit.Enqueue(n);
                }
            }
        }

        /// <summary>
        /// Marks a tile as belonging to the colony
        /// </summary>
        public void AddToColony(ColonyStructure structure)
        {
            PlanetTile tile = structure.Tile;
            ColonyRegion region;
            //get a list of the surrounding region colonies
            var surroundingIds = (from n in tile.GetAdjacentTiles() where n.ColonyRegionId > 0 select n.ColonyRegionId).Distinct().ToArray();
            //if there are no surrounding regions  then create a new one
            if (surroundingIds.Length == 0)
            {
                //add self as new colony region
                region = CreateRegion(tile);
            }
            else
            {
                //add the tile to the first adjacent region
                region = GetRegion(surroundingIds[0]);

                //if there is more than one adjacent region then combine them all into the first
                for (int i = 1; i < surroundingIds.Length; i++)
                {
                    ColonyRegion from = GetRegion(surroundingIds[i]);
                    CombineRegions(from, region);
                }
            }

            //add self to the region
            tile.ColonyRegionId = region.Id;
            region.Size++;

            //if (structure.NetPower > 0)
            //    region.GeneratedPower += structure.NetPower;
            //else if (structure.NetPower < 0)
            //    region.UsedPower += -structure.NetPower;
        }

        /// <summary>
        /// Called to remove the colony structure from a tile
        /// </summary>
        public void RemoveFromColony(ColonyStructure structure)
        {
            PlanetTile tile = structure.Tile;
            int regionId = tile.ColonyRegionId;
            //clear the region id of the tile
            tile.ColonyRegionId = 0;
            //get surrounding tiles that belong to the colony
            var surroundingColony = (from n in tile.GetAdjacentTiles() where n.ColonyRegionId == regionId select n).ToArray();
            //if no surrounding colony then this was a single tile region, so remove it
            if (surroundingColony.Length == 0)
            {
                RemoveRegion(regionId);
                return;
            }

            ColonyRegion region = GetRegion(regionId);

            //reduce the region size
            region.Size--;
            //reduce the power use of the structure
            if (structure.IsRunning)
            {
                region.UsedPower -= structure.PowerUsed;
                region.GeneratedPower -= structure.PowerGenerated;
            }

            //Change the region root tile to be the first surrounding tile. This guarantees the root tile will
            //remain in this region as the first surrounding tile is kept in the region
            region.Tile = surroundingColony[0];

            //if their is one neighbouring colony tile then just remove this tile from the region
            if (surroundingColony.Length == 1)
                return;

            //if there are two or more colony tiles surrounding this tile then potentially the removal of this colony tile causes
            //a split of the region into 2, 3 or 4 parts
            //go through each neighbour in turn
            HashSet<PlanetTile> toCheck = new HashSet<PlanetTile>(surroundingColony);
            HashSet<PlanetTile> visited = new HashSet<PlanetTile>();
            Queue<PlanetTile> toVisit = new Queue<PlanetTile>();
            bool first = true;
            for (int i = 0; i < surroundingColony.Length; i++)
            {
                PlanetTile t = surroundingColony[i];
                if (!toCheck.Contains(t))
                    continue;
                toCheck.Remove(t);

                visited.Clear();
                toVisit.Clear();
                toVisit.Enqueue(t);

                ColonyRegion newRegion = null;
                if (!first)
                    newRegion = CreateRegion(t);

                while (toVisit.Count > 0)
                {
                    PlanetTile visitedTile = toVisit.Dequeue();
                    if (toCheck.Contains(visitedTile))
                    {
                        toCheck.Remove(visitedTile);
                        //if all neighbours are still connected then and aren't constructing a new region
                        //then don't need to continue
                        if (toCheck.Count == 0 && first)
                            return;
                    }

                    if (newRegion != null)
                    {
                        visitedTile.ColonyRegionId = newRegion.Id;
                        newRegion.Size++;

                        ColonyStructure s = visitedTile.Structure as ColonyStructure;
                        if (s != null && s.IsRunning)
                        {
                            region.UsedPower -= s.PowerUsed;
                            region.GeneratedPower -= s.PowerGenerated;
                            newRegion.UsedPower += s.PowerUsed;
                            newRegion.GeneratedPower += s.PowerGenerated;
                        }
                    }

                    visited.Add(visitedTile);
                    foreach (var n in visitedTile.GetAdjacentTiles())
                    {
                        if (visited.Contains(n))
                            continue;
                        if (n.ColonyRegionId != regionId)
                            continue;
                        toVisit.Enqueue(n);
                    }
                }
                first = false;
            }
        }
    }
}
