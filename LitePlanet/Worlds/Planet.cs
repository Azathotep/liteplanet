﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LiteEngine.Rendering;
using LiteEngine.Math;
using LiteEngine.Physics;
using LitePlanet.Projectiles;
using LiteEngine.Procedural;
using LiteEngine.Textures;
using Microsoft.Xna.Framework.Graphics;
using LiteEngine.Core;
using LitePlanet.Structures;

namespace LitePlanet.Worlds
{
    public class Planet
    {
        static LiteEngine.Textures.Texture _basePlanetTexture = new LiteEngine.Textures.Texture("planets", new RectangleI(0, 0, 128, 128));
        static LiteEngine.Textures.Texture _cloudPlanetTexture = new LiteEngine.Textures.Texture("planets", new RectangleI(128, 0, 128, 128));

        static LiteEngine.Textures.Texture _pointTexture = new LiteEngine.Textures.Texture("point");
        static LiteEngine.Textures.Texture _particleTexture = new LiteEngine.Textures.Texture("particle");

        LiteEngine.Textures.Texture _grassTexture = new LiteEngine.Textures.Texture("brownplanet");
        int _width = 100;
        int _height;

        Biosphere _biosphere;

        public string Name;
        public string Description;
        public Color SurfaceColor = Color.Gray;
        public Color RockColor = Color.Gray;
        public Color AtmosphereColor = Color.White;
        public float AtmosphereAlpha = 0f;

        PlanetTile[,] _tiles;
        PlanetCollisionField _collisionField;

        public bool Dirty = true;
        int _crustDepth = 100;
        bool _grassOnTop = false;

        PlanetFarView _farView;

        PlanetFovHandler _fov;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius">radius of planet in tiles</param>
        public Planet(PhysicsCore physics, Vector2 position, int radius, bool grassOnTop=false)
        {
            _grassOnTop = grassOnTop;
            _physics = physics;
            _position = position;
            _width = (int)(radius * Math.PI * 2);
            _height = radius;

            _biosphere = new Biosphere(this);

            if (_physics != null)
            {
                GenerateGeometry();
            }
            _collisionField = new PlanetCollisionField(this);
            _fov = new PlanetFovHandler();

            _farView = new PlanetFarView(this);

        }

        internal Biosphere Biosphere
        {
            get
            {
                return _biosphere;
            }
        }

        internal PlanetCollisionField CollisionField
        {
            get
            {
                return _collisionField;
            }
        }

        internal PlanetFarView FarView
        {
            get
            {
                return _farView;
            }
        }

        public float MinX(float x1, float x2)
        {
            if (x2 > x1 + _width * 0.5f)
                return x2;
            if (x2 < x1 - _width * 0.5f)
                return x1;
            if (x1 < x2)
                return x1;
            return x2;
        }

        public float MaxX(float x1, float x2)
        {
            if (MinX(x1, x2) == x1)
                return x2;
            return x1;
        }

        void GenerateGeometry()
        {
            _tiles = new PlanetTile[_width, _crustDepth];

            NoiseField noise = new NoiseField(_width, _crustDepth);
            noise.GenerateRandomNoise();
            NoiseField nf = noise.GenerateOctave(0.1f, 8f);
            nf.Add(noise.GenerateOctave(0.8f, 1f));
            nf.Normalize();

            int surface = _crustDepth - 5;

            for (int y = 0; y < _crustDepth; y++)
                for (int x = 0; x < _width; x++)
                {
                    PlanetTile tile = new PlanetTile(this, x, _height - _crustDepth + y);
                    _tiles[x, y] = tile;

                    tile.Background = TileBackground.Sky;

                    if (y <= surface)
                    {
                        tile.Background = TileBackground.Cavern;
                        tile.AddBlock(BlockType.Rock);
                    }

                    //add grass at surface
                    if (y == surface)
                        tile.AddBlock(BlockType.Grass);

                    //carve out caverns
                    bool cavern = nf.Values[x, y] < 0.4f;
                    if (cavern)
                        tile.RemoveBlock();

                    //if (y < surface - 10)
                    //    if (LiteEngine.Core.Dice.Next(100) == 0)
                    //        type = TileBackground.Gold;
                    
                    if (y == _crustDepth - 1)
                        _tiles[x, y].Visible = true;

                    if (y == surface + 1 && _tiles[x, surface].BlockType == BlockType.Grass && LiteEngine.Core.Dice.Next(6) == 0)
                    {
                        tile.BlockType = BlockType.Tree;
                    }

                    if (y == surface - 15)
                    {
                        tile.WaterLevel = 0.5f;
                    }
                    if (y < surface - 15)
                        tile.WaterLevel = 1f;

                    if (y == surface + 1 && tile.BlockType == BlockType.None && x < 10)
                        //if (Dice.Next() < 0.01f)
                            tile.AddStructure(new Hive());
                }

            for (int i = 0; i < 10000; i++)
            {
                int tx = Dice.Next(_width);
                int ty = Radius + Dice.Next(10);

                Vector2 p = this.PolarToCartesian(new Vector2(tx, ty));
                _biosphere.AddCreature(p);
            }
        }

        Vector2 _position;
        public Vector2 Position
        {
            get
            {
                return _position;
            }
        }

        public void DrawIcon(XnaRenderer renderer, Vector2 position, float diameter, bool zoomedOut, float alpha = 1)
        {
            renderer.DrawDepth = 0.5f;
            renderer.DrawSprite(_particleTexture, Position, Vector2.One * Radius * 2.7f, 0, AtmosphereColor, 1f);
            renderer.DrawSprite(_pointTexture, Position, Vector2.One * Radius * 2.7f, 0, Color.White, 0f);


            if (zoomedOut)
            {
                renderer.DrawDepth = 0.2f; // 0.8f;
                renderer.DrawSprite(_basePlanetTexture, position, new Vector2(diameter, diameter), 0, SurfaceColor, alpha);
            }
                //renderer.DrawDepth = 0.4f;
            //renderer.DrawSprite(_cloudPlanetTexture, position, new Vector2(diameter * 1.1f, diameter * 1.1f), 0, AtmosphereColor, AtmosphereAlpha * alpha);
        }

        public int Radius
        {
            get
            {
                return _height;
            }
        }

        PhysicsCore _physics;
        public PhysicsCore Physics
        {
            get
            {
                return _physics;
            }
        }

        public Vector2 CartesianToPolar(Vector2 cCoords)
        { 
            Vector2 relcCoords = cCoords - Position;
            
            float y = (float)Math.Sqrt(relcCoords.X * relcCoords.X + relcCoords.Y * relcCoords.Y);
            float angle = (float)Math.Atan2(relcCoords.X, -relcCoords.Y);// +(float)Math.PI;
            //angle is now between 0 and 2.pi
            float x = angle / (2 * (float)Math.PI) * _width;
            return new Vector2(x, y); 
        }

        public Vector2 PolarToCartesian(Vector2 polar)
        {
            float angle = 2f * (float)Math.PI * polar.X / _width;
            float radius = polar.Y; // _width / (2f * (float)Math.PI) + polar.Y;
            float px = (float)Math.Sin(angle) * radius;
            float py = -(float)Math.Cos(angle) * radius;
            return new Vector2(px + Position.X, py + Position.Y);
        }

        void DrawTilesCloseup(XnaRenderer renderer, Camera2D camera, GameTime gameTime)
        {
            Vector2 polar = CartesianToPolar(camera.Position);
            for (int y = -30; y < 30; y++)
                for (int x = -50; x < 50; x++)
                {
                    PlanetTile tile = GetTile((int)polar.X + x, (int)polar.Y + y);
                    if (tile == null)
                        continue;
                    tile.Draw(gameTime, renderer);
                }
        }

        void DrawZoomOutView(XnaRenderer renderer, Camera2D camera, GameTime gameTime)
        {
            if (_farView.TileTexture == null)
                _farView.GenerateTileTexture(renderer.GraphicsDevice);

            //to fill screen
            if (_quadBuffer == null)
            {
                _quadBuffer = renderer.CreateVertexBuffer(4);
                VertexPositionColorTexture[] vertices = new VertexPositionColorTexture[4];
                vertices[0] = new VertexPositionColorTexture(new Vector3(camera.Position + new Vector2(3000, -3000), 0), Color.White, new Vector2(0, 0));
                vertices[1] = new VertexPositionColorTexture(new Vector3(camera.Position + new Vector2(-3000, -3000), 0), Color.White, new Vector2(1, 0));
                vertices[2] = new VertexPositionColorTexture(new Vector3(camera.Position + new Vector2(3000, 3000), 0), Color.White, new Vector2(1, 1));
                vertices[3] = new VertexPositionColorTexture(new Vector3(camera.Position + new Vector2(-3000, 3000), 0), Color.White, new Vector2(1, 1));
                _quadBuffer.SetData(vertices);
            }

            //each pixel of the tile texture holds information about a tile
            //the detail texture is a spritesheet with textures for all possible tiles
            Texture2D tileTexture = _farView.TileTexture;
            Texture2D detailTexture = renderer.ContentManager.Load<Texture2D>(@"textures\planet1"); //brownplanet");

            Effect effect = renderer.ContentManager.Load<Effect>("planet.mgfxo");
            effect.Parameters["xWorld"].SetValue(camera.World);
            effect.Parameters["xProjection"].SetValue(camera.Projection);
            effect.Parameters["xView"].SetValue(camera.View);

            effect.Parameters["xTexture"].SetValue(tileTexture);
            effect.Parameters["xTilesTexture"].SetValue(detailTexture);

            effect.Parameters["planetPos"].SetValue(Position);
            effect.Parameters["planetWidth"].SetValue(_width);
            effect.Parameters["zoom"].SetValue(renderer.ActiveCamera.Zoom);
            effect.Parameters["wav"].SetValue((float)Math.Sin((double)gameTime.TotalGameTime.TotalSeconds));

            //effect.Parameters["aa"].SetValue(fov._visibleField);
            //effect.Parameters["fovCenter"].SetValue(new Vector2(fov.X,fov.Y));

            effect.Techniques["Planet"].Passes[0].Apply();

            renderer.GraphicsDevice.SetVertexBuffer(_quadBuffer);
            renderer.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            renderer.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            renderer.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            renderer.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            renderer.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
        }

        VertexBuffer _quadBuffer = null;
        internal void Draw(XnaRenderer renderer, Camera2D camera, GameTime gameTime)
        {
            if (camera.Zoom < 1f)
            {
                Vector2 old = renderer.DrawOffset;
                DrawTilesCloseup(renderer, camera, gameTime);
                renderer.DrawOffset = old;
            }
            else
                DrawZoomOutView(renderer, camera, gameTime);

            _biosphere.Draw(renderer);
            
            //Corners corners2 = new Corners(camera.Position, camera.Position + new Vector2(10, 0), camera.Position + new Vector2(0, -10), camera.Position + new Vector2(10, -10));
        }

        internal PlanetFovHandler Fov
        {
            get
            {
                return _fov;
            }
        }

        /// <summary>
        /// Returns the distance of a point from the surface of the planet
        /// </summary>
        public float Altitude(Vector2 point)
        {
            Vector2 diff = point - Position;
            return diff.Length() - Radius;
        }

        static PlanetTile _lavaTile = new PlanetTile(null, 0, 0);

        public PlanetTile GetTile(int x, int y)
        {
            if (y >= _height)
                return null;
            if (y < 0 || y >= _height)
                return null;
            x = (x % _width + _width) % _width;
            if (y < _height - _crustDepth)
                return _lavaTile;
            int ry = y - (_height - _crustDepth);
            if (ry < 0 || ry >= _crustDepth)
                return null;
            return _tiles[x, ry];
        }

        internal void Update()
        {
            _biosphere.Update();
        }

        public float Width
        {
            get
            {
                return _width;
            }
        }

        List<Item> _items = new List<Item>();
        public List<Item> Items
        {
            get
            {
                return _items;
            }
        }

        internal void DropItem(Vector2 position)
        {
            Item item = new Item(this, _physics, position);
            _items.Add(item);
        }

        internal void RemoveItem(Item item)
        {
            _items.Remove(item);
        }

        Vector2I _nextThink = new Vector2I();

        internal void Think(Engine engine)
        {
            //get a random tile
            
            for (int i = 0; i < 1000; i++)
            {
                PlanetTile tile = _tiles[Dice.Next(_width), Dice.Next(_crustDepth)]; //_tiles[_nextThink.X, _nextThink.Y];
                tile.Update(engine);
                _nextThink.X++;
                if (_nextThink.X >= _width)
                {
                    _nextThink.X = 0;
                    _nextThink.Y++;
                    if (_nextThink.Y >= _crustDepth)
                        _nextThink.Y = 0;
                }

                
                //    PlanetTile neighbour = tile.GetNeighbour(Compass.GetRandomDirection());
                //    if (neighbour.Weed
                //}

                //if (tile.Health > 0)
                //{
                //    if (tile.Weed < 3)
                //        tile.Weed++;
                //}
            }
            //tile.Type = WorldTileType.Gold;
            //UpdateTile(tile);

            //CommitChanges();
        }

        ColonyRegionManager _colonyRegionManager;
        internal ColonyRegionManager ColonyRegionManager
        {
            get
            {
                return _colonyRegionManager;
            }
        }

        internal void RunFov(PlanetTile tile, int radius)
        {
            _fov.RunFov(this, tile.X, tile.Y, radius);
        }
    }

    public class ColonyRegion
    {
        public int Id;
        public int GeneratedPower;
        public int UsedPower;
        public int Size;
        //store a tile that belongs to the region as a way of obtaining all the other tiles
        public PlanetTile Tile;

        public ColonyRegion()
        {
            Color = Color.FromNonPremultiplied(new Vector4(Dice.Next(), Dice.Next(), Dice.Next(), 0.5f));
        }

        public Color Color { get; set; }
    }
}
