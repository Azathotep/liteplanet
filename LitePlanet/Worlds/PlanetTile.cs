﻿using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Physics;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LitePlanet.Creatures;
using LitePlanet.Projectiles;
using LitePlanet.Structures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    public class PlanetTile : IPhysicsObject, IDamageSink
    {
        TileCollisionBody _collisionBody;
        Planet _planet;
        TileBackground _background;
        int _x, _y;
        Vector2[] _vertices = new Vector2[4];
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="planet">planet the tile belongs to</param>
        /// <param name="x">X coordinate of tile in planet polar coordinates</param>
        /// <param name="y">Y coordinate of tile in planet polar coordinates</param>
        /// <param name="type"></param>
        public PlanetTile(Planet planet, int x, int y)
        {
            _planet = planet;
            _x = x;
            _y = y;
            _collisionBody = new TileCollisionBody(this);

            //generate the vertices of the tile's corners
            if (planet != null)
            {
                _vertices[0] = planet.PolarToCartesian(new Vector2(x, y + 1));
                _vertices[1] = planet.PolarToCartesian(new Vector2(x + 1, y + 1));
                _vertices[2] = planet.PolarToCartesian(new Vector2(x, y));
                _vertices[3] = planet.PolarToCartesian(new Vector2(x + 1, y));
            }
        }

        public PlanetTile GetNeighbour(CardinalDirection direction)
        {
            Vector2I dir = Compass.DirectionToVector2I(direction);
            return _planet.GetTile(_x + dir.X, _y + dir.Y);
        }

        public int X
        {
            get
            {
                return _x;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }
        }

        public Body Body
        {
            get
            {
                return _collisionBody.Body;
            }
        }

        public Vector2[] Vertices
        {
            get
            {
                return _vertices;
            }
        }

        public TileCollisionBody CollisionBody
        {
            get
            {
                return _collisionBody;
            }
        }

        public Planet Planet
        {
            get
            {
                return _planet;
            }
        }

        public TileBackground Background
        {
            get
            {
                return _background;
            }
            set
            {
                _background = value;
            }
        }

        BlockType _blockType = BlockType.None;
        public BlockType BlockType
        {
            get
            {
                return _blockType;
            }
            set
            {
                _blockType = value;
            }
        }

        Structure _structure = null;
        public Structure Structure
        {
            get
            {
                return _structure;
            }
            set
            {
                _structure = value;
            }
        }

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {
        }

        static int _weedammo = 3;

        byte _weedHealth = 0;

        int DamageWeeds(int damage)
        {
            if (damage > _weedHealth)
            {
                damage -= _weedHealth;
                _weedHealth = 0;
            }
            else
            {
                _weedHealth -= (byte)damage;
                damage = 0;
            }
            return damage;
        }

        int DamageStructure(int damage)
        {
            if (damage > _structureHealth)
            {
                damage -= _structureHealth;
                _structureHealth = 0;
                _structure.Remove();

                _collisionBody.DestroyBody();
            }
            else
            {
                _structureHealth -= (byte)damage;
                damage = 0;
            }
            return damage;
        }

        public void DamageBlock(int damageAmount)
        {
            if (damageAmount >= _blockHealth)
            {
                RemoveBlock();
                _collisionBody.DestroyBody();
            }
            else
            {
                _blockHealth -= (short)damageAmount;
            }
        }

        short _blockHealth;
        short _structureHealth;

        public void TakeDamage(int damageAmount)
        {
            //if (_weedammo > 0)
            //{
            //    _weedammo--;
            //    GrowWeeds();
            //    return;
            //}

            if (damageAmount < 1)
                return;

            //damage any weeds first
            if (_weedHealth > 0)
                damageAmount = DamageWeeds(damageAmount);

            if (damageAmount < 1)
                return;

            //damage any structure next
            if (Structure != null)
            {
                Structure.TakeDamage(damageAmount);

                //damageAmount = DamageStructure(damageAmount);
                //if (damageAmount < 1)
                //    return;
            }

            if (BlockType != BlockType.None)
            {
                DamageBlock(damageAmount);
                
            }

            //damage the tile itself
            //if (Type == PlanetTileType.Lava || Type == PlanetTileType.SolidRock)
            //    return;
            
            //Health = 0;
            //_planet.UpdateTile(this);
            //_planet.CommitChanges();
            //_collisionBody.DestroyBody();

            //if (Background == TileBackground.Gold)
            //{
            //    _planet.DropItem(_planet.PolarToCartesian(new Vector2(_x, _y)));
            //}
        }


        //Texture _blankTexture = new Texture(@"textures\planet1.blank");

        public bool Visible { get; set; }

        /// <summary>
        /// returns true if the background (sky, cavern) is visible through the block
        /// </summary>
        bool IsBackgroundVisible
        {
            get
            {
                if (_blockType == BlockType.None)
                    return true;
                return false;
            }
        }

        public void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
            //if tile is not visible then draw it black
            if (!Visible && _background != TileBackground.Sky)
            {
                renderer.DrawDepth = 0.1f;
                Texture texture = TextureBook.GetTexture(@"textures\planet1.blank");
                renderer.DrawSprite(texture, corners, Color.Black);
                return;
            }

 	        renderer.DrawDepth = 0.9f;
            if (IsBackgroundVisible)
                DrawBackground(renderer);

            renderer.DrawDepth = 0.7f;
            DrawBlock(gameTime, renderer);

            renderer.DrawDepth = 0.5f;
            if (_structure != null)
            {
                ColonyStructure colonyStructure = _structure as ColonyStructure;
                if (colonyStructure != null && colonyStructure.IsUnderConstruction)
                    colonyStructure.DrawConstruction(renderer);
                else
                    _structure.Draw(gameTime, renderer);
            }

            if (_weedHealth > 0)
                DrawWeeds(renderer);
            //tile.DrawForeground(renderer);

            renderer.DrawDepth = 0.2f;
            DrawOverlay(gameTime, renderer);
        }

        void DrawBackground(XnaRenderer renderer)
        {
            Color color = Color.White;
            Texture texture = null;

            if (_background == TileBackground.Sky)
                return;
            Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
            if (_background == TileBackground.Cavern)
            {
                texture = TextureBook.GetTexture(@"textures\planet1.cavern");
                //color = Color.SaddleBrown;
                renderer.DrawSprite(texture, corners, color);
            }
        }

        void DrawBlock(GameTime gameTime, XnaRenderer renderer)
        {
            renderer.DrawDepth = 0.7f;
            Texture texture=null;
            Color color = Color.White;
            switch (_blockType)
            {
                case BlockType.None:
                    return;
                case BlockType.Grass:
                    texture = TextureBook.GetTexture(@"textures\planet1.grass");
                    break;
                case BlockType.Rock:
                    color = Color.DarkGray;
                    texture = TextureBook.GetTexture(@"textures\planet1.rock");
                    break;
                case BlockType.Tree:
                    texture = TextureBook.GetTexture(@"textures\planet1.tree");
                    break;
            }
            if (texture == null)
                return;
            renderer.DrawSprite(texture, Corners, color);
        }

        public Corners Corners
        {
            get
            {
                return new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
            }
        }

        private void DrawWeeds(XnaRenderer renderer)
        {
            Color color = Color.White;
            Texture texture = null;
            if (_weedHealth > 20)
                texture = TextureBook.GetTexture(@"textures\planet1.weed3");
            else if (_weedHealth > 10)
                texture = TextureBook.GetTexture(@"textures\planet1.weed2");
            else if (_weedHealth > 0)
                texture = TextureBook.GetTexture(@"textures\planet1.weed1");
            if (texture != null)
            {
                Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
                renderer.DrawSprite(texture, corners, color);
            }
        }

        public float WaterLevel = 0;
        internal void DrawOverlay(GameTime gameTime, XnaRenderer renderer)
        {
            if (_colonyRegionId > 0)
            {
                Color ccolor = _planet.ColonyRegionManager.GetRegion(_colonyRegionId).Color;
                Texture ttexture = TextureBook.GetTexture(@"textures\planet1.blank");
                Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
                renderer.DrawSprite(ttexture, corners, ccolor);
                return;
            }


            if (WaterLevel <= 0)
                return;
            Color color = Color.White;
            Texture texture = null;
            if (WaterLevel > 0.5f)
            {
                color = Color.FromNonPremultiplied(new Vector4(0, 0.5f, 1, 0.5f));
                texture = TextureBook.GetTexture(@"textures\planet1.blank");
            }
            else if (WaterLevel > 0)
            {
                color = Color.FromNonPremultiplied(new Vector4(0, 0.5f, 1, 0.5f));
                if (gameTime.TotalGameTime.Milliseconds % 500 < 250)
                    texture = TextureBook.GetTexture(@"textures\planet1.liquid");
                else
                    texture = TextureBook.GetTexture(@"textures\planet1.liquid2");
            }
            if (texture != null)
            {
                Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
                renderer.DrawSprite(texture, corners, color);
            }
        }

        internal IEnumerable<PlanetTile> GetAdjacentTiles()
        {
            foreach (CardinalDirection direction in Compass.CardinalDirections)
            {
                var neighbour = GetNeighbour(direction);
                if (neighbour != null)
                    yield return neighbour;
            }
        }

        void GrowWeeds()
        {
            _weedHealth = (byte)Math.Min(_weedHealth + 10, 30);
        }

        internal void Update(Engine engine)
        {
            if (_structure != null)
                _structure.Think(engine);

            if (_weedHealth > 0)
                GrowWeeds();
            else
            {
                foreach (PlanetTile neighbour in GetAdjacentTiles())
                {
                    if (neighbour._weedHealth > _weedHealth && BlockType != Worlds.BlockType.None)
                        GrowWeeds();
                }
            }
        }

        public bool IsSolid
        {
            get
            {
                return _blockType != BlockType.None;
            }
        }

        public bool BlocksLight 
        {
            get
            {
                if (_blockType != BlockType.None)
                    return true;
                return false;
            }
        }

        int _colonyRegionId = 0;

        internal void DrawHighlight(XnaRenderer renderer, Color color)
        {
            color = Color.Multiply(color, 0.5f);
            Texture texture = TextureBook.GetTexture(@"textures\planet1.blank");
            Corners corners = new Corners(Vertices[0], Vertices[1], Vertices[2], Vertices[3]);
            renderer.DrawSprite(texture, corners, color);
        }

        public int ColonyRegionId
        {
            get
            {
                return _colonyRegionId;
            }
            set
            {
                _colonyRegionId = value;
            }
        }

        /// <summary>
        /// Returns a neighbour relative to this tile
        /// </summary>
        internal PlanetTile GetNeighbour(int x, int y)
        {
            return _planet.GetTile(X + x, Y + y);
        }

        public bool IsEmpty
        {
            get
            {
                if (IsSolid)
                    return false;
                if (Structure != null)
                    return false;
                return true;
            }
        }

        internal void AddBlock(BlockType blockType)
        {
            _blockType = blockType;
            switch (blockType)
            {
                case BlockType.Grass:
                    _blockHealth = 20;
                    break;
                case BlockType.Rock:
                    _blockHealth = 50;
                    break;
            }
        }

        internal void AddStructure(Structure structure)
        {
            _structure = structure;
            _structure.Tile = this;
        }

        internal void RemoveBlock()
        {
            _blockHealth = 0;
            _blockType = BlockType.None;
        }

        public bool CollisionUseFlag { get; set; }
    }

    public class Item : IPhysicsObject
    {
        Body _body;
        Planet _planet;
        public Item(Planet planet, PhysicsCore physics, Vector2 position)
        {
            _planet = planet;
            _body = physics.CreateBody(this);

            _body.Enabled = false;
            
            Fixture f = FixtureFactory.AttachCircle(0.3f, 1f, _body, Vector2.Zero);
            _body.BodyType = BodyType.Dynamic;
            _body.Mass = 0.0001f;
            _body.CollisionCategories = Category.Cat3;
            _body.CollidesWith = Category.Cat1 | Category.Cat2;
            _body.Position = position;
            _body.LinearVelocity = Dice.RandomVector2(0.1f);
            _body.Enabled = true;
        }

        public void Draw(XnaRenderer renderer)
        {
            Texture texture = new Texture("particle");
            renderer.DrawSprite(texture, _body.Position, new Vector2(0.3f, 0.3f), 0, Color.Yellow, 0.8f); 
        }

        public Body Body
        {
            get { return _body; }
        }

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {
            //throw new NotImplementedException();
        }

        internal void Remove()
        {
            _planet.Physics.RemoveBody(_body);
            _planet.RemoveItem(this);
        }
    }

    /// <summary>
    /// Manages the farseer static collision body object for a tile.
    /// </summary>
    public class TileCollisionBody
    {
        Body _body;
        PlanetTile _tile;

        static Queue<Body> _unusedBodies = new Queue<Body>();

        public TileCollisionBody(PlanetTile tile)
        {
            _tile = tile;
        }

        public void CreateBody()
        {
            if (_tile.Planet == null)
                return;
            if (_unusedBodies.Count > 0)
            {
                _body = _unusedBodies.Dequeue();
            }
            else
            {
                PhysicsCore physics = _tile.Planet.Physics;
                _body = physics.CreateBody(_tile);
                Vertices v = new Vertices(_tile.Vertices);
                Fixture f = FixtureFactory.AttachPolygon(v, 1, _body, null);
            }
            ((PolygonShape)_body.FixtureList[0].Shape).Vertices = new Vertices(_tile.Vertices);

            _body.UserData = _tile;
            _body.Restitution = -0.5f;
            _body.Friction = 0.5f;
            _body.IsStatic = true;
            _body.Enabled = true;
            
        }

        public void DestroyBody()
        {
            if (_body != null)
            {
                _body.Enabled = false;
                //_tile.Planet.Physics.RemoveBody(_body);
                _unusedBodies.Enqueue(_body);
                _body = null;
            }
        }

        public Body Body
        {
            get
            {
                return _body;
            }
        }
    }

    
    public class MiningDrill : ColonyStructure
    {
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture = null;
            if (gameTime.TotalGameTime.Milliseconds < 250)
                texture = TextureBook.GetTexture(@"textures\planet1.drill1");
            else if (gameTime.TotalGameTime.Milliseconds < 500)
                texture = TextureBook.GetTexture(@"textures\planet1.drill2");
            else if (gameTime.TotalGameTime.Milliseconds < 750)
                texture = TextureBook.GetTexture(@"textures\planet1.drill3");
            else
                texture = TextureBook.GetTexture(@"textures\planet1.drill4");
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
        }
    }

    public class Housing : ColonyStructure
    {
        float amt = 1;
        bool tx = false;
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            
            if (Dice.Next(100) == 0)
                tx = !tx;
            Texture texture = TextureBook.GetTexture(@"textures\planet1.housing");
            if (tx)
                texture = TextureBook.GetTexture(@"textures\planet1.housing2");
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
            renderer.DrawDepth = 0.3f;
            renderer.DrawPoint(new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.8f), 2, Color.FromNonPremultiplied(new Vector4(1f, 1f, 0f, 0.3f)), 0f);
            base.Draw(gameTime, renderer);
        }

        protected override Texture ConstructionTexture
        {
            get
            {
                return TextureBook.GetTexture(@"textures\planet1.houseConstruction"); ;
            }
        }

        public override int PowerUsed
        {
            get
            {
                return 2;
            }
        }

        public override void Think(Engine engine)
        {
            _colony.GrowPopulation();
            base.Think(engine);
        }
    }

    class Turret : ColonyStructure
    {
        float _angle;
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            _angle = 0.3f;
            //_angle += 0.01f;

            renderer.DrawSprite(TextureBook.GetTexture(@"textures\planet1.drill1"), Tile.Corners, Color.White);

            renderer.DrawDepth = 0.4f;
            Texture texture = TextureBook.GetTexture(@"textures\planet1.turret");

            renderer.DrawSprite(texture, new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.1f), new Vector2(1f, 1f), _angle);
            
            //Texture barrel = TextureBook.GetTexture(@"textures\planet1.blank");
            //renderer.DrawSprite(barrel, new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.5f), new Vector2(3f, 0.5f), _angle);
            
            base.Draw(gameTime, renderer);
        }

        public override void Think(Engine engine)
        {
            
            Vector2 fireDirection = Util.AngleToVector(_angle);
            //engine.Shells.CreateShell(new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.1f) + fireDirection, fireDirection * 30f);

            engine.CreateMissile(new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.1f) + fireDirection, fireDirection * 20f); 

            base.Think(engine);
        }

        public override short MaxHealth
        {
            get
            {
                return 10;
            }
        }
    }

    class Mine : ColonyStructure
    {
        public override short MaxHealth
        {
            get
            {
                return 0;
            }
        }

        public override void Think(Engine engine)
        {
            if (IsUnderConstruction)
            {
                Tile.DamageBlock(30);
                _colony.Ore += 5;
                if (Tile.BlockType == BlockType.None)
                {
                    //block has been fully mined
                    IsUnderConstruction = false;
                    //expand the view
                    Tile.Planet.RunFov(Tile, 5);
                    foreach (var t in Tile.GetAdjacentTiles())
                    {
                        if (t.Structure != null && t.Structure.GetType() == typeof(Mine))
                            ((Mine)(t.Structure)).CalculateTexture();
                    }

                    CalculateTexture();
                }
            }
        }

        void CalculateTexture()
        {
            string textureName = "";
            //remember south and north are the wrong way round...
            if (Tile.GetNeighbour(CardinalDirection.South).BlockType != BlockType.None)
                textureName += "n";
            if (Tile.GetNeighbour(CardinalDirection.East).BlockType != BlockType.None)
                textureName += "e";
            if (Tile.GetNeighbour(CardinalDirection.North).BlockType != BlockType.None)
                textureName += "s";
            if (Tile.GetNeighbour(CardinalDirection.West).BlockType != BlockType.None)
                textureName += "w";
            if (textureName.Length == 0)
            {
                //no surrounding blocks so remove mine
                Remove();
                return;
            }
            
            _texture = TextureBook.GetTexture(@"textures\planet1.mine_" + textureName);
        }

        Texture _texture = null;
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture = _texture;
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
            base.Draw(gameTime, renderer);
        }
    }

    public class DrillTop : ColonyStructure
    {
        static Animation _animation = new Animation();
        static DrillTop()
        {
            _animation.AddFrame(TextureBook.GetTexture(@"textures\planet1.drilltop1"), 250);
            _animation.AddFrame(TextureBook.GetTexture(@"textures\planet1.drilltop2"), 250);
            _animation.AddFrame(TextureBook.GetTexture(@"textures\planet1.drilltop1"), 250);
            _animation.AddFrame(TextureBook.GetTexture(@"textures\planet1.drilltop3"), 250);
            _animation.Loop = true;
        }

        public override void Think(Engine engine)
        {
            //if (_isRunning && Dice.Next(10) == 0)
            //    Deeper();
            base.Think(engine);
        }

        public override int PowerUsed
        {
            get
            {
                return 2;
            }
        }

        void Deeper()
        {
            PlanetTile tile = Tile;
            while (true)
            {
                tile = tile.GetNeighbour(CardinalDirection.North);
                if (tile == null)
                    break;
                if (tile.Structure == null)
                {
                    tile.BlockType = BlockType.None;
                    _colony.Build(StructureType.Drill, tile);
                    foreach (var n in tile.GetAdjacentTiles())
                        n.Visible = true;
                    break;
                }
            }
        }

        static long _animLastRun = 0;
        
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture=null;
            
            if (_isRunning)
            {
                texture = _animation.CurrentTexture;
                if (gameTime.TotalGameTime.Ticks != _animLastRun)
                {
                    _animation.Speed = _colony.ProductionEfficiency;
                    _animation.Advance(gameTime.ElapsedGameTime.Milliseconds);
                    _animLastRun = gameTime.TotalGameTime.Ticks;
                }
            }
            else
            {
                texture = TextureBook.GetTexture(@"textures\planet1.drilltop1");
            }

            

            //if (gameTime.TotalGameTime.Milliseconds < 250 || !_isRunning)
            //    texture = TextureBook.GetTexture(@"textures\planet1.mine");
            //else if (gameTime.TotalGameTime.Milliseconds < 500)
            //    texture = TextureBook.GetTexture(@"textures\planet1.mine2");
            //else if (gameTime.TotalGameTime.Milliseconds < 750)
            //    texture = TextureBook.GetTexture(@"textures\planet1.mine");
            //else
            //    texture = TextureBook.GetTexture(@"textures\planet1.mine3");
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
        }
    }

    //determines the background type of the tile
    public enum TileBackground
    {
        Sky,
        Cavern
        //Earth,
        //Rock,
        //SolidRock,
        //Gold,
        //Lava,
        //Sky
    }

    public enum BlockType
    {
        None,
        Grass,
        Rock,
        Tree
    }
}

