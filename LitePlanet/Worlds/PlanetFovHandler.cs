﻿using LiteEngine.Fov;
using LiteEngine.Math;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    /// <summary>
    /// Manages FOV calculations on a planet
    /// </summary>
    internal class PlanetFovHandler : IFovInfo
    {
        RecursiveShadowcast _fov = new RecursiveShadowcast();
        Planet _planet;

        public bool TileBlocksLight(int x, int y)
        {
            PlanetTile tile = _planet.GetTile(x, y);
            if (tile == null)
                return false;
            return tile.BlocksLight;
        }
        public float[] _visibleField = new float[100];

        List<PlanetTile> _visibleTiles = new List<PlanetTile>();
        List<PlanetTile> _oldVisibleTiles = new List<PlanetTile>();

        public void OnTileVisible(int x, int y)
        {
            PlanetTile tile = _planet.GetTile(x, y);
            if (tile != null)
            {
                if (!tile.Visible)
                {
                    tile.Visible = true;
                    _planet.FarView.UpdateTile(tile);
                    _visibleTiles.Add(tile);
                }
            }
        }

        public int X;
        public int Y;
        public void RunFov(Planet planet, int x, int y, int viewRadius)
        {
            //for (int i = 0; i < _visibleField.Length; i++)
            //    _visibleField[i] = 0;
            X = x;
            Y = y;
            _planet = planet;


            //foreach (PlanetTile tile in _visibleTiles)
            //{
            //    tile.Visible = false;
            //    _planet.UpdateTile(tile);
            //}

            _visibleTiles.Clear();
            _fov.CalculateFov(new Vector2I(x, y), viewRadius, this);

            _planet.FarView.CommitChanges();
        }
    }
}
