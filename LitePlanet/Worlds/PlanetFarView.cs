﻿using LiteEngine.Textures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    internal class PlanetFarView
    {
        Planet _planet;
        public PlanetFarView(Planet planet)
        {
            _planet = planet;
        }

        Microsoft.Xna.Framework.Graphics.Texture2D _texture;
        public Microsoft.Xna.Framework.Graphics.Texture2D TileTexture
        {
            get
            {
                return _texture;
            }
        }

        int _chunkWidth = 8;
        int _numHorizontalChunks;
        public void GenerateTileTexture(Microsoft.Xna.Framework.Graphics.GraphicsDevice device)
        {
            int bmWidth = ((int)(_planet.Width / _chunkWidth) + 1) * _chunkWidth;
            _numHorizontalChunks = bmWidth / _chunkWidth;
            int height = 512;
            _texture = new Microsoft.Xna.Framework.Graphics.Texture2D(device, bmWidth, height, false, Microsoft.Xna.Framework.Graphics.SurfaceFormat.Color);
            byte[] pixels = new byte[bmWidth * height * 4];

            for (int y = 0; y < height; y++)
                for (int x = 0; x < _planet.Width; x++)
                {
                    int i = (y * bmWidth + x) * 4;
                    PlanetTile tile = _planet.GetTile(x, y);
                    Color tileValue = GetTileValue(tile);
                    pixels[i] = tileValue.R;
                    pixels[i + 1] = tileValue.G;
                    pixels[i + 2] = tileValue.B;
                    pixels[i + 3] = tileValue.A;
                }
            _texture.SetData<byte>(pixels);
        }

        LiteEngine.Textures.Texture GetTileDetailTexture(PlanetTile tile)
        {
            switch (tile.BlockType)
            {
                case BlockType.Grass:
                    return TextureBook.GetTexture(@"textures\planet1.grass");
                case BlockType.Rock:
                    return TextureBook.GetTexture(@"textures\planet1.rock");
                case BlockType.Tree:
                    return TextureBook.GetTexture(@"textures\planet1.tree");
            }
            if (tile.Background == TileBackground.Cavern)
                return TextureBook.GetTexture(@"textures\planet1.cavern");
            return TextureBook.GetTexture(@"textures\planet1.transparent");
        }

        Color GetTileValue(PlanetTile tile)
        {
            Color ret = new Color(0, 0, 0, 0);
            if (tile == null)
                return ret;

            ret.R = 255;

            float tx = 0;
            float ty = 0;

            LiteEngine.Textures.Texture detailTexture = GetTileDetailTexture(tile);
            if (detailTexture != null)
            {
                tx = (float)detailTexture.Bounds.Value.X / 238;
                ty = (float)detailTexture.Bounds.Value.Y / 238;
            }

            ret.G = (byte)(tx * 255);
            ret.B = (byte)(ty * 255);

            ret.A = 255;
            if (!tile.Visible)
                ret.A = 0;
            else
                ret.A = 255;
            return ret;
        }

        HashSet<int> _modifiedChunks = new HashSet<int>();
        internal void UpdateTile(PlanetTile tile)
        {
            int chunkX = tile.X / _chunkWidth;
            int chunkY = tile.Y / _chunkWidth;
            int chunkId = chunkY * _numHorizontalChunks + chunkX;
            _modifiedChunks.Add(chunkId);
        }

        byte[] _chunkData = new byte[8 * 8 * 4];
        public void CommitChanges()
        {
            foreach (int chunkId in _modifiedChunks)
            {
                int chunkOffset = chunkId * _chunkWidth * _chunkWidth * 4;
                int chunkY = chunkId / _numHorizontalChunks;
                int chunkX = chunkId % _numHorizontalChunks;
                int d = 0;
                for (int y = 0; y < _chunkWidth; y++)
                    for (int x = 0; x < _chunkWidth; x++)
                    {
                        int tileX = chunkX * _chunkWidth + x;
                        int tileY = chunkY * _chunkWidth + y;
                        PlanetTile tile = _planet.GetTile(tileX, tileY);
                        Color tileValue = GetTileValue(tile);

                        _chunkData[d] = tileValue.R;
                        _chunkData[d + 1] = tileValue.G;
                        _chunkData[d + 2] = tileValue.B;
                        _chunkData[d + 3] = tileValue.A;
                        d += 4;
                    }
                Rectangle r = new Rectangle(chunkX * _chunkWidth, chunkY * _chunkWidth, _chunkWidth, _chunkWidth);
                _texture.SetData<byte>(0, r, _chunkData, 0, 0);
            }
            _modifiedChunks.Clear();
        }
    }
}
