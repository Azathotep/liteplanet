﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Physics;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Worlds
{
    class Biosphere
    {
        BiosphereBucket[,] _buckets;
        ActiveRegion _activeRegion;
        Planet _planet;

        int _numBucketsX;

        public Biosphere(Planet planet)
        {
            _activeRegion = new ActiveRegion(this);
            _planet = planet;

            _numBucketsX = (planet.Radius * 2) / _bucketWidth + 6;
            _buckets = new BiosphereBucket[_numBucketsX, _numBucketsX];
            for (int y = 0; y < _numBucketsX; y++)
                for (int x = 0; x < _numBucketsX; x++)
                    _buckets[x, y] = new BiosphereBucket();
        }

        public Planet Planet
        {
            get
            {
                return _planet;
            }
        }

        internal void Update()
        {
            _activeRegion.UpdateCreatures();
            UpdateStaticCreatures();

        }
        
        int _bucketWidth = 20;

        public int BucketWidth
        {
            get
            {
                return _bucketWidth;
            }
        }

        public BiosphereBucket GetBucket(Vector2 position)
        {
            int bucketX = (int)Math.Floor(position.X / _bucketWidth) / (_numBucketsX / 2);
            int bucketY = (int)Math.Floor(position.Y / _bucketWidth) + (_numBucketsX / 2);
            if (bucketX < 0 || bucketX >= _numBucketsX)
                return null;
            if (bucketY < 0 || bucketY >= _numBucketsX)
                return null;
            return _buckets[bucketX, bucketY];
        }

        void UpdateStaticCreatures()
        {
            float x = Dice.RandomFloat(1f);
            if (x >= 0)
                x = (float)Math.Sqrt(x);
            else
                x = -(float)Math.Sqrt(-x);
            float y = Dice.RandomFloat(1f);
            if (y >= 0)
                y = (float)Math.Sqrt(y);
            else
                y = -(float)Math.Sqrt(-y);

            x *= 100;
            y *= 100;

            Vector2 position = _activeRegion.Center + new Vector2(x, y);
            BiosphereBucket bucket = GetBucket(position);
            if (bucket != null)
                bucket.UpdateCreatures();
        }

        public ActiveRegion ActiveRegion
        {
            get
            {
                return _activeRegion;
            }
        }

        public void AddCreature(Vector2 position)
        {
            BiosphereBucket bucket = GetBucket(position);
            if (bucket != null)
            {
                Creature creature = new Creature();
                creature.Position = position;
                bucket.Add(creature);
            }
        }

        internal void Draw(XnaRenderer renderer)
        {
            _activeRegion.DrawCreatures(renderer);
        }
    }

    class BiosphereBucket
    {
        HashSet<Creature> _creatures = new HashSet<Creature>();
        public void UpdateCreatures()
        {
            foreach (Creature creature in _creatures)
                creature.StaticUpdate();
        }

        public HashSet<Creature> Creatures
        {
            get
            {
                return _creatures;
            }
        }

        internal void Add(Creature creature)
        {
            _creatures.Add(creature);
        }

        internal void Remove(Creature creature)
        {
            _creatures.Remove(creature);
        }
    }

    class ActiveRegion
    {
        HashSet<Creature> _creatures = new HashSet<Creature>();
        public Vector2 Center;

        Biosphere _biosphere;
        public ActiveRegion(Biosphere biosphere)
        {
            _biosphere = biosphere;
        }

        public void FindNewCreatures()
        {
            for (int y = -1; y <= 1; y++)
                for (int x = -1; x <= 1; x++)
                {
                    BiosphereBucket bucket = _biosphere.GetBucket(Center + new Vector2(x, y) * _biosphere.BucketWidth);
                    if (bucket == null)
                        continue;

                    _toRemove.Clear();
                    foreach (Creature creature in bucket.Creatures)
                        if (CreatureInRange(creature))
                        {
                            _creatures.Add(creature);
                            creature.CreateBody(_biosphere.Planet.Physics);
                            _toRemove.Add(creature);
                        }
                    foreach (Creature creature in _toRemove)
                        bucket.Remove(creature);
                }
        }

        public HashSet<Creature> Creatures
        {
            get
            {
                return _creatures;
            }
        }

        bool CreatureInRange(Creature creature)
        {
            float distSq = (creature.Position - Center).LengthSquared();
            if (distSq <= 500)
                return true;
            return false;
        }

        List<Creature> _toRemove = new List<Creature>();
        public void UpdateCreatures()
        {
            _toRemove.Clear();
            foreach (Creature creature in _creatures)
            {
                creature.Update();
                if (!CreatureInRange(creature))
                    _toRemove.Add(creature);
            }
            foreach (Creature creature in _toRemove)
            {
                creature.RemoveBody(_biosphere.Planet.Physics);
                _creatures.Remove(creature);
                BiosphereBucket bucket = _biosphere.GetBucket(creature.Position);
                if (bucket == null)
                    continue;
                bucket.Add(creature);
            }
        }

        internal void DrawCreatures(XnaRenderer renderer)
        {
            foreach (Creature creature in _creatures)
                creature.Draw(renderer);
        }
    }

    class Creature : IPhysicsObject
    {
        float _rotation;
        public void Update()
        {
            if (float.IsNaN(Position.X))
                return;
            Vector2 move = Dice.RandomVector2(0.1f);
            Position += move;
            move.Normalize();
            _rotation = Util.AngleBetween(new Vector2(0,-1), move);
        }

        Vector2 _position;
        public Vector2 Position
        {
            get
            {
                if (_body != null)
                    return _body.Position;
                return _position;
            }
            set
            {
                if (float.IsNaN(value.X))
                {
                    int a = 1;
                }
                _position = value;
                if (_body != null)
                    _body.Position = value;
            }
        }

        internal void StaticUpdate()
        {
            Position += Dice.RandomVector2(0.1f);
        }

        internal void Draw(XnaRenderer renderer)
        {
            Texture texture = TextureBook.GetTexture(@"textures\planet1.bee");
            float facing = _body.Rotation;
            //if (_target != null)
            //{
            //    //facing = Util.AngleBetween(_body.Position, _target.Position);
            //}
            renderer.DrawSprite(texture, Body.Position, new Vector2(0.5f, 0.5f), _rotation);

            //Texture texture = TextureBook.GetTexture(@"textures\planet1.hive");
            //renderer.DrawSprite(texture, Position, new Vector2(0.5f,0.5f), 0f);
        }

        Body _body = null;

        internal void CreateBody(PhysicsCore physics)
        {
            _body = physics.CreateBody(this);
            _body.Enabled = false;
            _body.BodyType = BodyType.Dynamic;
            _body.AngularDamping = 0.5f;
            _body.Friction = 1f;
            _body.Restitution = 1.1f;
            _body.Mass = 0.5f;
            _body.Rotation = 0f;

            _body.Position = _position;
            if (float.IsNaN(Position.X))
            {
                int a = 1;
            }
            FixtureFactory.AttachCircle(0.1f, 1f, _body);
            
            _body.CollisionCategories = Category.Cat2;
            _body.CollidesWith = Category.Cat1 | Category.Cat2 | Category.Cat3;
            _body.Enabled = true;
        }

        public void RemoveBody(PhysicsCore physics)
        {
            if (float.IsNaN(_position.X))
            {
                int a = 1;
            }

            _position = _body.Position;
            physics.RemoveBody(_body);
            _body = null;
            if (float.IsNaN(Position.X))
            {
                int a = 1;
            }
        }

        public Body Body
        {
            get { return _body; }
        }

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {
            
        }
    }
}
