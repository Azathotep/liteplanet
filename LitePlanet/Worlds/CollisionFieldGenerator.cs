﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using LiteEngine.Math;

namespace LitePlanet.Worlds
{
    class PlanetCollisionField
    {
        bool _currentFlagValue = true;
        Planet _planet;
        public PlanetCollisionField(Planet planet)
        {
            _planet = planet;
        }

        public void Begin()
        {
            _newInuseList.Clear();
        }

        List<PlanetTile> _oldInuseList = new List<PlanetTile>();
        List<PlanetTile> _newInuseList = new List<PlanetTile>();
        public void UseTile(PlanetTile tile)
        {
            if (tile.BlockType == BlockType.None && tile.Structure == null)
                return;

            tile.CollisionUseFlag = _currentFlagValue;
            if (tile.CollisionBody.Body == null)
                tile.CollisionBody.CreateBody();
            _newInuseList.Add(tile);
        }

        public void CreateCollisionTilesAroundPoint(Vector2 position)
        {
            Vector2 polar = _planet.CartesianToPolar(position);
            for (int y = -2; y <= 2; y++)
                for (int x = -2; x <= 2; x++)
                {
                    PlanetTile tile = _planet.GetTile((int)polar.X + x, (int)polar.Y + y);
                    if (tile == null)
                        continue;
                    UseTile(tile);
                }
        }

        public void End()
        {
            foreach (PlanetTile tile in _oldInuseList)
            {
                if (tile.CollisionUseFlag != _currentFlagValue)
                    tile.CollisionBody.DestroyBody();
            }
            _currentFlagValue = !_currentFlagValue;
            _oldInuseList = _newInuseList;
        }
    }
}
