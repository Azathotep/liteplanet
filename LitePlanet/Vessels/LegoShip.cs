﻿using LiteEngine.Core;
using LiteEngine.Particles;
using LitePlanet.ShipComponents;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Vessels
{
    class LegoShip : Ship
    {
        ShipLayout _layout;
        int _power = 20;

        public LegoShip(Engine engine) : base(engine)
        {
            _layout = new ShipLayout(this);
        }

        public ShipLayout Layout
        {
            get
            {
                return _layout;
            }
        }

        /// <summary>
        /// Returns the amount of power the ship has accumulated
        /// </summary>
        public int Power
        {
            get
            {
                return _power;
            }
        }

        internal override void Update()
        {
            foreach (ShipGridCell cell in _layout.Grid)
                if (cell != null && cell.Component != null)
                    cell.Component.Update();
            base.Update();
        }


        public override void Fire(Engine engine)
        {
            foreach (ShipGridCell cell in _layout.Grid)
            {
                if (cell == null)
                    continue;
                ShipComponent component = cell.Component;
                if (component == null)
                    continue;
                if (component.GetType() == typeof(Blaster))
                {
                    Vector2 offset = new Vector2((cell.X - 1) * 0.2f, (cell.Y - 1) * 0.2f);
                    offset = Vector2.Transform(offset, Matrix.CreateRotationZ(Rotation));
                    Vector2 componentPosition = Position + offset;
                    Vector2 direction = Facing;
                    Blaster blaster = component as Blaster;
                    blaster.Fire(engine);
                }
            }
        }

        internal bool DrawPower(int power)
        {
            if (_power < power)
                return false;
            _power -= power;
            return true;
        }

        internal void SupplyPower(int power)
        {
            _power += power;
        }

        internal void FireForwardEngines(Engine engine)
        {
            foreach (ShipGridCell cell in _layout.Grid)
            {
                if (cell == null || cell.Component == null)
                    continue;
                ShipComponent component = cell.Component;
                if (component == null)
                    continue;
                SmokerEngine smoker = component as SmokerEngine;
                if (smoker != null)
                {
                    smoker.Fire(engine);
                }
            }
        }
    }
}
