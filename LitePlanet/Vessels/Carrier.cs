﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using LiteEngine.Core;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.Math;
using LiteEngine.Physics;
using LiteEngine.Particles;
using FarseerPhysics.Common;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics;
using LitePlanet.Effects;
using LitePlanet.Weapons;
using LitePlanet.Projectiles;
using LitePlanet.Worlds;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics.Contacts;
using LitePlanet.UI;
using LitePlanet.Comms;

namespace LitePlanet.Vessels
{
    class Carrier : Ship
    {
        static Texture _texture = new Texture("largeship", new RectangleI(128-38,0, 38, 128));
        static Texture _redTexture = new Texture("redship");
        static Texture _circleTexture = new Texture("circleOverlay");
        Cannon _cannon;
        Turret[] _turrets = new Turret[3];
        public Carrier(Engine engine) : base(engine)
        {
            _cannon = new Cannon();
            //Body.Mass = 10;
            _hull = 10000;
            _maxSpeed = 20;

            for (int i = 0; i < 3; i++)
            {
                Vector2 turretPosition = new Vector2(0f, (i - 1) * 3.5f);
                _turrets[i] = new Turret(this, turretPosition);
            }

            _engineMaxThrust = 10f;
        }

        protected override Body CreateBody()
        {
            Body body = _engine.Physics.CreateBody(this);
            body.BodyType = BodyType.Dynamic;
            body.AngularDamping = 0.5f;
            body.Friction = 0f;
            body.Restitution = 1.1f;
            body.Mass = 10f;
            body.Rotation = 0f;
            body.LinearVelocity = new Vector2(0, 0);


            //FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(-1.5f, -6f), new Vector2(1.5f, -6f), new Vector2(1.5f, 6f), new Vector2(-1.5f, 6f) }), 1f, body);

            FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(-1.5f, -6f), new Vector2(1.5f, -6f), new Vector2(1.5f, -1f), new Vector2(-1.5f, -1f) }), 1f, body);
            FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(-1.5f, 1f), new Vector2(1.5f, 1f), new Vector2(1.5f, 6f), new Vector2(-1.5f, 6f) }), 1f, body);
            
            var f = FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(-0.4f, -1f), new Vector2(0.4f, -1f), new Vector2(0.4f, 1f), new Vector2(-0.4f, 1f) }), 1f, body);
            f.IsSensor = true;
            f.OnCollision += new OnCollisionEventHandler(OnCollision);
            f.OnSeparation += new OnSeparationEventHandler(OnSeparation);
            //FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(0f, -0.4f), new Vector2(0.35f, 0.4f), new Vector2(-0.35f, 0.4f) }), 1f, body);
            //FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(0f, 1.2f), new Vector2(0.35f, 0.4f), new Vector2(-0.35f, 0.4f) }), 1f, body);
            body.CollisionCategories = Category.Cat2;
            body.CollidesWith = Category.Cat1 | Category.Cat2;
            return body;
        }

        int _dockCooldown = 0;

        bool d = false;
        bool OnCollision(Fixture f1, Fixture f2, Contact contact)
        {

            Ship ship = f2.Body.UserData as Ship;
            if (ship == null)
                return true;
            if (_dockCooldown > 0)
                return false;
            DockShip(ship);
            d = true;
            return false;
        }

        void OnSeparation(Fixture f1, Fixture f2)
        {
            Ship ship = f2.Body.UserData as Ship;
            if (ship == null)
                return;
            //DockShip(ship);
            return;
        }

        void DockShip(Ship ship)
        {
            ship.DockedWith = this;
            //cannot set ship.Body.Enabled to false here because this method is called during a physics step after a collision
            //and disabling the body involved in the collision here causes a null reference exception in the physics engine
            //instead the ship disables it's own body during the next update tick

            _engine.CameraTarget = this;

            Hud.Instance.MessageWindow.AddMessage(Message.FromEvent("Now docked with the " + Info.Name));
            SendMessage("Hello, Welcome aboard the " + Info.Name);

            //Hud.Instance.ShowDockView(this);
        }

        public void LaunchShip(Ship ship)
        {
            ship.DockedWith = null;
            ship.Position = new Vector2(Position.X, Position.Y);
            
            Vector2 launchVelocity = Vector2.Transform(new Vector2(5, 0), Matrix.CreateRotationZ(Rotation));
            ship.Velocity = Velocity + launchVelocity;
            ship.Rotation = Rotation + (float)Math.Atan2(5, 0);
            ship.Body.AngularVelocity = 0;
            //Body.IgnoreCollisionWith(ship.Body);
            ship.Body.Enabled = true;
            _dockCooldown = 100;
        }

        public void DrawCollisionArea(XnaRenderer renderer)
        {
            Microsoft.Xna.Framework.Graphics.Effect effect = renderer.ContentManager.Load<Microsoft.Xna.Framework.Graphics.Effect>("basicshader.mgfxo");

            Matrix world = renderer.ActiveCamera.World;
            world = Matrix.Multiply(Matrix.CreateRotationZ(Rotation), Matrix.CreateTranslation(new Vector3(Position, 0)));
            effect.Parameters["xWorld"].SetValue(world);
            effect.Parameters["xProjection"].SetValue(renderer.ActiveCamera.Projection);
            effect.Parameters["xView"].SetValue(renderer.ActiveCamera.View);
            Microsoft.Xna.Framework.Graphics.Texture2D xnaTexture = renderer.ContentManager.Load<Microsoft.Xna.Framework.Graphics.Texture2D>("grass");
            effect.Parameters["TextureSampler"].SetValue(xnaTexture);
            effect.Techniques["Basic"].Passes[0].Apply();


            List<Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture> vertices = new List<Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture>();
            foreach (Fixture f in Body.FixtureList)
            {
                PolygonShape ps = f.Shape as PolygonShape;
                for (int i=2;i<ps.Vertices.Count;i++)
                {
                    vertices.Add(new Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture(new Vector3(ps.Vertices[0].X, ps.Vertices[0].Y, 0f), Color.Red, new Vector2(0f, 0f)));
                    vertices.Add(new Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture(new Vector3(ps.Vertices[i - 1].X, ps.Vertices[i - 1].Y, 0f), Color.Red, new Vector2(1f, 0f)));
                    vertices.Add(new Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture(new Vector3(ps.Vertices[i].X, ps.Vertices[i].Y, 0f), Color.Red, new Vector2(1f, 1f)));
                }
            }
            Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture[] buffer = vertices.ToArray();
            renderer.GraphicsDevice.DrawUserPrimitives<Microsoft.Xna.Framework.Graphics.VertexPositionColorTexture>(Microsoft.Xna.Framework.Graphics.PrimitiveType.TriangleList,
                                                                buffer, 0, buffer.Length / 3);
        }

        float[] _turretRot = new float[3];
        int[] _turretSpinDir = { 1, 1, 1 };
        Vector2[] _turretTarget = { Vector2.One, Vector2.One, Vector2.One };
        int f = 0;
        public override void Draw(XnaRenderer renderer)
        {
            if (_hull <= 0)
                return;

            Texture texture = _texture;
            renderer.DrawDepth = 0.5f;
            renderer.DrawSprite(texture, Position, new Vector2(3f, 12f), Rotation);
            renderer.DrawDepth = 0.2f;

            f++;
            foreach (Turret turret in _turrets)
            {
                turret.Draw(renderer);
            }

            float power = _enginePercent;
            Texture t = new Texture("particle");
            Color c = Color.Cyan;
            Vector2 d = Dice.RandomVector2(0.02f);
            c *= 0.4f * power;
            renderer.DrawSprite(t, Position - Facing * 5.5f + d, new Vector2(1.2f, 0.8f), Rotation, c, 0);
            c = Color.White;
            c *= 0.6f * power;
            renderer.DrawSprite(t, Position - Facing * 5.5f + d, new Vector2(1f, 0.4f) * power, Rotation, c, 0);
        }

        internal override void DrawDockArrows(XnaRenderer renderer, bool away)
        {
            if (_dockCooldown > 0)
                return;
            renderer.DrawDepth = 0.8f;
            float animX = (f % 20) * 0.05f;
            float arrowDir;
            Vector2 arrowPos;
            if (away)
            {
                arrowDir = Rotation + (float)Math.PI * 0.5f;
                arrowPos = new Vector2(2f + animX, 0f);
            }
            else
            {
                arrowDir = Rotation - (float)Math.PI * 0.5f;
                arrowPos = new Vector2(3f - animX, 0f);
            }
            arrowPos = Vector2.Transform(arrowPos, Matrix.CreateRotationZ(Rotation));
            renderer.DrawSprite(new Texture("triangleOverlay"), new Vector2(Position.X + arrowPos.X, Position.Y + arrowPos.Y), new Vector2(1f, 1f), arrowDir, Color.Cyan, 1f);
            renderer.DrawDepth = 0.5f;
        }

        static Texture _outlineTexture = new Texture("triangleOverlay");// new Texture("shipparts", new RectangleI(0, 0, 32, 32));
        public override void DrawIcon(XnaRenderer renderer, float width, bool emphasis=false)
        {
            Color c = Color.FromNonPremultiplied(new Vector4(0, 1, 0, 1));
            if (emphasis)
            {
                width *= 1.1f;
                c = Color.White;
            }
            renderer.DrawSprite(_outlineTexture, Position, new Vector2(1f, 2f) * width, Rotation, c, 0f);
        }

        public void TurnTurretTowards(Turret turret, Vector2 target)
        {
            float a = Util.AngleBetween(turret.Position, target);
            float angle = Util.AngleBetween(turret.Rotation, a);
            if (angle > 0.1f)
                turret.Rotate(0.01f);
            else if (angle < -0.1f)
                turret.Rotate(-0.01f);
            
            //if (Math.Abs(angle) < 0.2f && Dice.Next(20) == 0 && Vector2.Distance(Position, target) < 40f)
            //    turret.Fire(_engine);
        }

        internal void Target(Ship ship)
        {
            foreach (Turret t in _turrets)
                TurnTurretTowards(t, ship.Position);
        }

        internal override void Update()
        {
            for (int i=0;i<3;i++)
            {
                TurnTurretTowards(_turrets[i], _turretTarget[i]);
            }
            if (Dice.Next(300) == 0)
            {
                _turretTarget[Dice.Next(3)] = Position + Dice.RandomVector2(10);
            }

            if (_dockCooldown > 0)
                _dockCooldown--;

            ApplyForwardThrust(0.2f);
            ApplyRotateThrust(6f);
            base.Update();
        }

        internal void SendMessage(string message)
        {
            Hud.Instance.MessageWindow.AddMessage(Message.FromCommunication(this, message));
        }
    }

    class Turret
    {
        static Texture _turretTexture = new Texture("largeship", new RectangleI(0, 0, 32, 32));
        Cannon _cannon = new Cannon();
        Ship _owner;
        float _relativeRotation;
        Vector2 _relativePosition;

        public Turret(Ship owner, Vector2 relativePosition)
        {
            _owner = owner;
            _relativePosition = relativePosition;
        }

        public Vector2 Position
        {
            get
            {
                Vector2 ret = _owner.Position + Vector2.Transform(_relativePosition, Matrix.CreateRotationZ(_owner.Rotation));
                return ret;
            }
        }

        public float Rotation
        {
            get
            {
                return _relativeRotation + _owner.Rotation;
            }
        }

        public void Rotate(float amount)
        {
            _relativeRotation += amount;
        }

        public void Draw(XnaRenderer renderer)
        {
            float totalRot = Rotation;
            renderer.DrawSprite(_turretTexture, Position, new Vector2(2f, 2f), totalRot);
        }

        internal void Fire(Engine engine)
        {
            float totalRot = Rotation;
            _cannon.Fire(engine, _owner, Position, new Vector2((float)Math.Sin(totalRot), (float)-Math.Cos(totalRot)));
        }
    }
}
