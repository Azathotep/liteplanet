﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using LiteEngine.Core;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LiteEngine.Math;
using LiteEngine.Physics;
using LiteEngine.Particles;
using FarseerPhysics.Common;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics;
using LitePlanet.Effects;
using LitePlanet.Weapons;
using LitePlanet.Projectiles;
using LitePlanet.Worlds;
using LitePlanet.ShipComponents;

namespace LitePlanet.Vessels
{
    class Ship : IPhysicsObject, IDamageSink, ITargetable
    {
        List<ShipComponent> _components = new List<ShipComponent>();
        Cannon _cannon;
        Body _body;
        static Texture _texture = new Texture("rocketship");
        static Texture _redTexture = new Texture("redship");
        static Texture _circleTexture = new Texture("circleOverlay");
        static Texture _triangleTexture = new Texture("triangleOverlay");

        TargetInfo _info;

        protected float _engineMaxThrust = 5f;

        protected Engine _engine;
        bool _hostile;
        protected float _maxSpeed = 2050;

        public Ship(Engine engine, bool other=false)
        {
            _engine = engine;
            _cannon = new Cannon(10);
            _body = CreateBody();
            _hull = 100;
            if (other)
                _hostile = true;
            if (_hostile)
                _hull = 100;
        }

        public float MaxSpeed
        {
            get
            {
                return _maxSpeed;
            }
            set
            {
                _maxSpeed = value;
            }
        }

        public Ship DockedWith
        {
            get;
            set;
        }

        public void SetInfo(TargetInfo info)
        {
            _info = info;
        }

        public virtual void Fire(Engine engine)
        {

        }

        protected virtual Body CreateBody()
        {
            Body body = _engine.Physics.CreateBody(this);
            body.BodyType = BodyType.Dynamic;
            body.AngularDamping = 0.5f;
            body.Friction = 1f;
            body.Restitution = 1.1f;
            body.Mass = 0.5f;
            body.Rotation = 0f;
            FixtureFactory.AttachPolygon(new Vertices(new Vector2[] { new Vector2(0f, -0.4f), new Vector2(0.35f, 0.4f), new Vector2(-0.35f, 0.4f) }), 1f, body);
            body.CollisionCategories = Category.Cat2;
            body.CollidesWith = Category.Cat1 | Category.Cat2 | Category.Cat3;
            return body;
        }

        public Body Body
        {
            get
            {
                return _body;
            }
        }

        public Cannon PrimaryWeapon
        {
            get
            {
                return _cannon;
            }
        }


        protected int _hull = 300;
        public int Hull
        {
            get
            {
                return _hull;
            }
        }

        float _jumpCharge = 0;
        public float JumpDriveCharge
        {
            get
            {
                return _jumpCharge;
            }
        }

        public bool JumpDriveCharging
        {
            get
            {
                return _jumpDriveOn;
            }
        }

        bool _jumpDriveOn = false;
        public void Jump(bool on)
        {
            _jumpDriveOn = on;
        }

        int _fuel = 100000;
        public int Fuel
        {
            get
            {
                return _fuel;
            }
            set
            {
                _fuel = value;
            }
        }

        public Vector2 Position
        {
            get 
            {
                return _body.Position;
            }
            set
            {
                _body.Position = value;
            }
        }

        public Vector2 Velocity
        {
            get 
            {
                return _body.LinearVelocity;
            }
            set
            {
                _body.LinearVelocity = value;
            }
        }

        public Vector2 Facing
        {
            get 
            {
                return _body.GetWorldVector(new Vector2(0, -1));
            }
        }

        public float Rotation
        {
            get 
            {
                return _body.Rotation;
            }
            set
            {
                _body.Rotation = value;
            }
        }


        protected float _enginePercent;
        float _engineTargetPercent;
        internal void ActivateEngines(float percent=1)
        {
            ApplyForwardThrust(1f);

            Vector2 enginePosition = Position - Facing * 0.6f;
            Vector2 vel = Velocity - Facing * 5.1f;
            vel.X += Dice.Next() * 1.6f - 0.8f;
            vel.Y += Dice.Next() * 1.6f - 0.8f;
            Particle exhaust = _engine.ExhaustParticles.CreateParticle(enginePosition, vel, 50);
            if (exhaust == null)
                return;
            exhaust.Body.CollidesWith = Category.Cat1;
            exhaust.Body.CollisionCategories = Category.Cat6;
        }

        int s = 0;
        public void ApplyForwardThrust(float amount)
        {
            _body.ApplyForce(Facing * amount);
            float len = _body.LinearVelocity.LengthSquared();

            if (len > _maxSpeed)
                _body.LinearVelocity *= _maxSpeed / len;
        }

        public void ApplyRotateThrust(float amount)
        {
            _body.ApplyTorque(amount);
        }

        public virtual void Draw(XnaRenderer renderer)
        {
            if (_hull <= 0)
                return;
            Texture texture = _texture;
            if (_hostile)
                texture = _redTexture;
            renderer.DrawDepth = 0.7f;
            renderer.DrawSprite(texture, Position, new Vector2(1f, 1f), Rotation);
        }

        public virtual void DrawIcon(XnaRenderer renderer, float width, bool emphasis=false)
        {
            Color c = Color.FromNonPremultiplied(new Vector4(0, 1, 0, 1));
            if (_hostile)
                c = Color.Red;
            if (emphasis)
            {
                width *= 1.1f;
                c = Color.White;
            }
            renderer.DrawSprite(_triangleTexture, Position, new Vector2(width, width), Rotation, c, 0f);
        }

        public int Gold = 0;

        public void OnCollideWith(IPhysicsObject self, IPhysicsObject other, float impulse)
        {
            if (other is Item)
            {
                Item item = other as Item;
                item.Remove();
                _fuel += 100;
                Gold++;
                return;
            }

            TakeDamage((int)Math.Pow(impulse * 2, 2));

            //IDamageSink sink = other as IDamageSink;
            //sink.TakeDamage((int)impulse * 2);
        }

        public void TakeDamage(int damageAmount)
        {
            _hull -= damageAmount;
            if (_hull < 0)
            {
                _hull = 0;
                _fuel = 0;
                Explosion explosion = new Explosion(_engine);
                explosion.Create(Position);
            }
        }

        internal void RemoveBody()
        {
            _engine.Physics.RemoveBody(_body);
        }

        internal virtual void Update()
        {
            //if (_enginePercent < _engineTargetPercent)
            //    _enginePercent = Math.Min(_enginePercent + 0.01f, _engineTargetPercent);
            //else
            //    _enginePercent = Math.Max(_enginePercent - 0.01f, 0);
            //float thrust = _engineMaxThrust * _enginePercent;

            //if (_fuel <= 0)
            //    return;
            //_fuel -= (int)thrust;
            //if (_fuel < 0)
            //    _fuel = 0;

            //ApplyForwardThrust(thrust);

            if (IsDocked && Body.Enabled)
                Body.Enabled = false;

            Vector2 vel = Velocity - Facing * 5.1f;
            vel.X += Dice.Next() * 1.6f - 0.8f;
            vel.Y += Dice.Next() * 1.6f - 0.8f;

            if (_body.Mass > 1)
                return;
            //Vector2 enginePosition = Position - Facing * 0.6f;
            //float power = _enginePercent;
            //if (Dice.Next() < power)
            //{
            //    Particle exhaust = _engine.ExhaustParticles.CreateParticle(enginePosition, vel, 50);
            //    if (exhaust == null)
            //        return;
            //    exhaust.Body.CollidesWith = Category.Cat1;
            //    exhaust.Body.CollisionCategories = Category.Cat6;
            //}

            //_engineTargetPercent = 0;
            //_enginePercent = 0;


            //Vector2 p = enginePosition + Dice.RandomVector(0.3f); // Position - Facing * 0.7f 
            //_engine.SmokeParticles.CreateParticle(p, Velocity * 0, 50);
        }

        Vector2 ITargetable.Location
        {
            get { return Position; }
        }


        public TargetInfo Info
        {
            get { return _info; }
        }

        public bool IsDocked
        {
            get
            {
                return DockedWith != null;
            }
        }

        /// <summary>
        /// Draws arrows pointing towards or away from any docking points on the ship
        /// </summary>
        /// <param name="renderer"></param>
        /// <param name="away">if true the arrows point out of the docks</param>
        internal virtual void DrawDockArrows(XnaRenderer renderer, bool away)
        {
            
        }
    }
}
