﻿using LiteEngine.Core;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LitePlanet.Worlds;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    public class ColonyStructure : Structure
    {
        protected Colony _colony;
        short _health = 10;

        public virtual int PowerGenerated
        {
            get
            {
                return 0;
            }
        }

        public virtual int PowerUsed
        {
            get
            {
                return 0;
            }
        }

        public virtual short MaxHealth
        {
            get
            {
                return 100;
            }
        }

        public override void Think(Engine engine)
        {
            if (IsUnderConstruction)
            {
                _health += 10;
                if (_health >= MaxHealth)
                {
                    _health = MaxHealth;
                    IsUnderConstruction = false;
                    OnConstructionComplete();
                }
            }
            base.Think(engine);
        }

        protected virtual void OnConstructionComplete()
        {
            Start();
        }

        protected bool _isRunning = false;
        public void Stop()
        {
            if (!_isRunning)
                return;
            Colony.PowerGenerated -= PowerGenerated;
            Colony.PowerRequired -= PowerUsed;
            _isRunning = false;
        }

        public void Start()
        {
            if (_isRunning)
                return;
            Colony.PowerGenerated += PowerGenerated;
            Colony.PowerRequired += PowerUsed;
            _isRunning = true;
        }

        public virtual void OnBuilt()
        {

        }

        public bool IsRunning { get { return _isRunning; } }

        internal Colony Colony
        {
            get
            {
                return _colony;
            }
            set
            {
                _colony = value;
            }
        }

        public override void Remove()
        {
            _colony.RemoveStructure(this);
            base.Remove();
        }

        public void DrawConstruction(XnaRenderer renderer)
        {
            if (ConstructionTexture != null)
            {
                float buildProportion = (float)_health / MaxHealth;
                Corners corners = new Corners(new Vector2(Tile.Vertices[2].X, Tile.Vertices[2].Y - buildProportion), new Vector2(Tile.Vertices[3].X, Tile.Vertices[3].Y - buildProportion), Tile.Vertices[2], Tile.Vertices[3]);
                renderer.DrawSprite(ConstructionTexture, corners, Color.White);
            }

            //draw cranes
            renderer.DrawDepth = 0.4f;
            renderer.DrawSprite(TextureBook.GetTexture(@"textures\planet1.construction"), Tile.Corners, Color.White);
            //welding flashes
            if (Dice.Next(2) == 0)
            {
                renderer.DrawDepth = 0.3f;
                renderer.DrawPoint(new Vector2(Tile.Vertices[0].X + 0.5f, Tile.Vertices[0].Y + 0.6f), 1, Color.FromNonPremultiplied(new Vector4(0f, 1f, 1f, 0.5f)), 0f);
            }
        }

        protected virtual Texture ConstructionTexture
        {
            get
            {
                return null;
            }
        }

        public bool IsUnderConstruction = true;
    }
}
