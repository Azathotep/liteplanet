﻿using LiteEngine.Rendering;
using LitePlanet.Worlds;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    public abstract class Structure
    {
        protected short _health;

        public virtual void Think(Engine engine)
        {

        }

        public virtual void Draw(GameTime gameTime, XnaRenderer renderer)
        {
        }

        public PlanetTile Tile { get; set; }

        public virtual void Remove()
        {
            Tile.Structure = null;
            Tile = null;
        }

        internal void TakeDamage(int damageAmount)
        {
            if (damageAmount > _health)
            {
                _health = 0;
                Remove();
                return;
            }
            OnDamaged();
            _health -= (short)damageAmount;
        }

        protected virtual void OnDamaged()
        {

        }
    }
}
