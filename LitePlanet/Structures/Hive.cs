﻿using LiteEngine.Core;
using LiteEngine.Rendering;
using LiteEngine.Textures;
using LitePlanet.Creatures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    class Hive : Structure
    {
        public Hive()
        {
            _health = 200;
        }

        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture = TextureBook.GetTexture(@"textures\planet1.hive");
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
        }

        public override void Think(Engine engine)
        {
            //if (Dice.Next() < 0.1f)
            //{
            //    Bee bee = new Bee(engine.Physics);
            //    bee.Body.Position = Tile.Corners.TopLeft;
            //    engine.Bees.Add(bee);
            //}

            base.Think(engine);
        }

        protected override void OnDamaged()
        {
            while (Globals.Engine.Bees.Count < 5)
            {
                Bee bee = new Bee(Globals.Engine.Physics);
                bee.Body.Position = Tile.Corners.TopLeft;
                if (float.IsNaN(bee.Body.Position.X))
                {
                    int a = 1;
                }
                Globals.Engine.Bees.Add(bee);
                bee.SetTarget(Globals.Engine.PlayerInfo.Ship);
            }
        }
    }
}
