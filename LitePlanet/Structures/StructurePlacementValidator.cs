﻿using LitePlanet.Worlds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    abstract class StructurePlacementValidator
    {
        public abstract bool Validate(PlanetTile anchorTile);
        public string ErrorMessage = "";
    }

    class BasicStructurePlacementValidator : StructurePlacementValidator
    {
        public override bool Validate(PlanetTile tile)
        {
            ErrorMessage = "";
            if (tile.Structure != null)
                return false;
            if (tile.IsSolid)
                return false;
            //tile below must be solid
            if (!tile.GetNeighbour(0, -1).IsSolid)
            {
                ErrorMessage = "Tile beneath must be solid";
                return false;
            }

            //look for a nearby structure. Can only build near existing structures
            int radius = 2;
            for (int y = -radius; y <= radius; y++)
                for (int x = -radius; x <= radius; x++)
                {
                    if (x == 0 && y == 0)
                        continue;
                    PlanetTile t = tile.GetNeighbour(x, y);
                    if (t != null && t.Structure != null && !((ColonyStructure)t.Structure).IsUnderConstruction)
                        return true;
                }
            ErrorMessage = "Cannot build more than two tiles from a completed structure";
            return false;
        }
    }

    class MinePlacementValidator : StructurePlacementValidator
    {
        public override bool Validate(PlanetTile tile)
        {
            ErrorMessage = "";
            if (tile.Structure != null)
                return false;
            if (!tile.IsSolid)
            {
                ErrorMessage = "A mine can only be placed on a solid tile";
                return false;
            }
            ////tile below must be solid
            //if (!tile.GetNeighbour(0, -1).IsSolid)
            //{
            //    ErrorMessage = "Tile beneath must be solid";
            //    return false;
            //}

            //look for a nearby structure. Can only build near existing structures
            int radius = 2;
            for (int y = -radius; y <= radius; y++)
                for (int x = -radius; x <= radius; x++)
                {
                    if (x == 0 && y == 0)
                        continue;
                    PlanetTile t = tile.GetNeighbour(x, y);
                    if (t != null && t.Structure != null && !((ColonyStructure)t.Structure).IsUnderConstruction)
                        return true;
                }
            ErrorMessage = "Cannot build more than two tiles from a completed structure";
            return false;
        }
    }
}
