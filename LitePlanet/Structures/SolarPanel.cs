﻿using LiteEngine.Rendering;
using LiteEngine.Textures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{

    public class SolarPanel : ColonyStructure
    {
        public override void Draw(GameTime gameTime, XnaRenderer renderer)
        {
            Texture texture = TextureBook.GetTexture(@"textures\planet1.solarpanel");
            renderer.DrawSprite(texture, Tile.Corners, Color.White);
        }

        public override int PowerGenerated
        {
            get
            {
                return 1;
            }
        }

        public override short MaxHealth
        {
            get
            {
                return 20;
            }
        }

        protected override Texture ConstructionTexture
        {
            get
            {
                return TextureBook.GetTexture(@"textures\planet1.solarpanel");
            }
        }

        public override void Think(Engine engine)
        {
            base.Think(engine);
        }
    }
}
