﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    public enum StructureType
    {
        Mine,
        DrillTop,
        SolarPanel,
        Drill,
        Housing,
        None,
        Turret
    }
}
