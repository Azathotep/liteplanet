﻿using LiteEngine.Textures;
using LitePlanet.Worlds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.Structures
{
    class StructureFactory
    {
        public static ColonyStructure CreateStructure(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.Mine:
                    return new Mine();
                case StructureType.DrillTop:
                    return new DrillTop();
                case StructureType.SolarPanel:
                    return new SolarPanel();
                case StructureType.Drill:
                    return new MiningDrill();
                case StructureType.Housing:
                    return new Housing();
                case StructureType.Turret:
                    return new Turret();
            }
            return null;
        }

        public static Texture GetIcon(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.Mine:
                    return TextureBook.GetTexture(@"textures\planet1.mine");
                case StructureType.DrillTop:
                    return TextureBook.GetTexture(@"textures\planet1.drilltop1");
                case StructureType.SolarPanel:
                    return TextureBook.GetTexture(@"textures\planet1.solarpanel");
                case StructureType.Housing:
                    return TextureBook.GetTexture(@"textures\planet1.houseConstruction");
                case StructureType.Turret:
                    return TextureBook.GetTexture(@"textures\planet1.turret");
            }
            return null;
        }

        public static string GetName(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.Mine:
                    return "Mine";
                case StructureType.DrillTop:
                    return "Drill Top";
                case StructureType.SolarPanel:
                    return "Solar Farm";
                case StructureType.Housing:
                    return "Housing Dome";
            }
            return "";
        }

        public static string GetDescription(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.DrillTop:
                    return "Mine";
                case StructureType.SolarPanel:
                    return "Provides +1 power, but only in daylight";
                case StructureType.Housing:
                    return "Provides living space for 20 colonists";
            }
            return "";
        }

        public static int GetOreCost(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.DrillTop:
                    return 30;
                case StructureType.SolarPanel:
                    return 10;
                case StructureType.Housing:
                    return 30;
            }
            return 0;
        }

        static BasicStructurePlacementValidator _basicValidator = new BasicStructurePlacementValidator();
        static MinePlacementValidator _mineValidator = new MinePlacementValidator();

        internal static StructurePlacementValidator GetPlacementValidator(StructureType structure)
        {
            switch (structure)
            {
                case StructureType.Mine:
                    return _mineValidator;
                case StructureType.DrillTop:
                case StructureType.SolarPanel:
                case StructureType.Housing:
                case StructureType.Turret:
                    return _basicValidator;
            }
            return null;
        }
    }
}
