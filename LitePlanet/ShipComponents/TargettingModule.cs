﻿using LitePlanet.Worlds;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    class TargettingModule
    {
        ITargetable _currentTarget;
        
        public void NextTarget(StarSystem system, Vector2 origin, ITargetable ignore)
        {
            float currentDistance = 0f;
            if (_currentTarget != null)
                currentDistance = (_currentTarget.Location - origin).Length();
            ITargetable next = (from s in system.Ships where s != ignore && (s.Position - origin).Length() > currentDistance orderby (s.Position - origin).Length() select (ITargetable)s).FirstOrDefault();
            if (next == null)
                next = (from s in system.Ships where s != ignore orderby (s.Position - origin).Length() select s).FirstOrDefault();
            _currentTarget = next;
        }

        public ITargetable CurrentTarget
        {
            get
            {
                return _currentTarget;
            }
        }
    }

    interface ITargetable
    {
        Vector2 Location
        {
            get;
        }

        TargetInfo Info
        {
            get;
        }
    }

    class TargetInfo
    {
        public string Name;
        public string Type = "";
        public bool CanDock = false;
    }
}
