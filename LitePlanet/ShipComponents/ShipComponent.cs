﻿using LiteEngine.Textures;
using LitePlanet.Vessels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    public abstract class ShipComponent
    {
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int BaseCost { get; set; }
        public ShipComponent(int health)
        {
            Health = health;
            MaxHealth = health;
            BaseCost = 100;
        }

        public abstract string Name
        {
            get;
        }
        public abstract Texture Image
        {
            get;
        }
        public int Size;

        public abstract string ShortDescription
        {
            get;
        }

        public virtual string[] GenerateStatusText()
        {
            return new string[0];
        }

        internal virtual void Update()
        {
        }

        /// <summary>
        /// If the component is installed this property will be set to the location it is installed in
        /// </summary>
        internal ShipGridCell InstallPoint { get; set; }

        internal LegoShip Ship
        {
            get
            {
                if (InstallPoint == null)
                    return null;
                return InstallPoint.Layout.Ship;
            }
        }
    }
}
