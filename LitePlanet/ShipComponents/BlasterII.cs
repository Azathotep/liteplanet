﻿using LiteEngine.Math;
using LiteEngine.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    public class BlasterII : ShipComponent
    {
        public BlasterII() : base(15)
        {
            BaseCost = 200;
        }

        public override string Name
        {
            get { return "Blaster Cannon Mk II"; }
        }

        public override string ShortDescription
        {
            get { return "Front mounted cannon"; }
        }

        public override Texture Image
        {
            get { return new Texture("shipparts", new RectangleI(96, 0, 32, 32)); }
        }

        public override string[] GenerateStatusText()
        {
            string[] ret = new string[] 
            {
                "Ammo Remaining: 420/600",
                "Ammo Type: projectile",
                "Rate of fire: 10/second",
                "Damage: low"
            };
            return ret;
        }
    }
}
