﻿using LitePlanet.Vessels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    class ShipLayout
    {
        LegoShip _ship;
        ShipGridCell[,] _grid;

        public ShipLayout(LegoShip ship)
        {
            _ship = ship;
        }

        public LegoShip Ship
        {
            get
            {
                return _ship;
            }
        }

        public void Initialize(int width, int height)
        {
            _width = width;
            _height = height;
            _grid = new ShipGridCell[width, height];
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                    _grid[x, y] = new ShipGridCell(this, x, y);
        }

        int _width;
        public int Width
        {
            get
            {
                return _width;
            }
        }
        int _height;
        public int Height
        {
            get
            {
                return _height;
            }
        }

        public void RemoveCell(int x, int y)
        {
            _grid[x, y] = null;
        }

        public void InstallComponent(int x, int y, ShipComponent component)
        {
            if (_grid[x, y] == null)
                _grid[x, y] = new ShipGridCell(this, x, y);
            _grid[x, y].InstallComponent(component);
        }

        public ShipGridCell[,] Grid
        {
            get
            {
                return _grid;
            }
        }
    }

    class ShipGridCell
    {
        ShipComponent _installedComponent;
        private int _x;
        private int _y;
        ShipLayout _layout;

        public ShipGridCell(ShipLayout layout, int x, int y)
        {
            _layout = layout;
            _x = x;
            _y = y;
        }

        public ShipLayout Layout
        {
            get
            {
                return _layout;
            }
        }

        public int X
        {
            get
            {
                return _x;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }
        }

        public ShipComponent Component
        {
            get
            {
                return _installedComponent;
            }
        }

        internal void InstallComponent(ShipComponent component)
        {
            _installedComponent = component;
            component.InstallPoint = this;
        }

        internal void RemoveComponent()
        {
            _installedComponent.InstallPoint = null;
            _installedComponent = null;
        }
    }
}
