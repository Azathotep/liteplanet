﻿using FarseerPhysics.Dynamics;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Particles;
using LiteEngine.Textures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    public class SmokerEngine : ShipComponent
    {
        public SmokerEngine() : base(10)
        {
            BaseCost = 600;
        }

        public override string Name
        {
            get { return "Smoker"; }
        }

        public override string ShortDescription
        {
            get { return "Low cost but inefficient Hydrocarbon fuel engine"; }
        }

        public override Texture Image
        {
            get { return new Texture("shipparts", new RectangleI(32, 0, 32, 32)); }
        }

        public override string[] GenerateStatusText()
        {
            string[] ret = new string[]
            {
                "Fuel Remaining: 1100/1400",
                "Fuel Type:      Hydrocarbon",
                "Fuel Use:       10/second",
                "Power Used:     1",
                "Speed:          Slow"
            };
            return ret;
        }

        internal void Fire(Engine engine)
        {
            if (Ship.Fuel == 0)
                return;
            Ship.Fuel--;
            Ship.ApplyForwardThrust(5);

            Vector2 enginePosition = Ship.Position - Ship.Facing * 0.5f;
            Vector2 vel = Ship.Velocity - Ship.Facing * 5.1f;
            vel.X += Dice.Next() * 1.6f - 0.8f;
            vel.Y += Dice.Next() * 1.6f - 0.8f;

            Particle exhaust = engine.ExhaustParticles.CreateParticle(enginePosition, vel, 50);
            if (exhaust == null)
                return;
            exhaust.Body.CollidesWith = Category.Cat1;
            exhaust.Body.CollisionCategories = Category.Cat6;
        }
    }
}
