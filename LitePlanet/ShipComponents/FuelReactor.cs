﻿using LiteEngine.Math;
using LiteEngine.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    public class FuelReactor : ShipComponent
    {
        public FuelReactor() : base(10)
        {
            BaseCost = 800;
        }

        public override string Name
        {
            get { return "Fuel Reactor"; }
        }

        public override string ShortDescription
        {
            get { return "Ship reactor which converts fuel to power"; }
        }

        public override Texture Image
        {
            get { return new Texture("shipparts", new RectangleI(0, 32, 32, 32)); }
        }

        public override string[] GenerateStatusText()
        {
            string[] ret = new string[]
            {
                "Power Generated: 5",
                "Fuel Use:       1/second"
            };
            return ret;
        }

        int _frame = 0;
        internal override void Update()
        {
            _frame++;
            if (_frame > 90)
            {
                if (Ship.Fuel > 1 && Ship.Power < 30)
                {
                    Ship.Fuel--;
                    Ship.SupplyPower(1);
                    _frame = 0;
                }
            }
        }
    }
}
