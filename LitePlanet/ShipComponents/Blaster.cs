﻿using FarseerPhysics.Dynamics;
using LiteEngine.Core;
using LiteEngine.Math;
using LiteEngine.Particles;
using LiteEngine.Textures;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePlanet.ShipComponents
{
    public class Blaster : ShipComponent
    {
        public Blaster() : base(10)
        {
            BaseCost = 100;
        }

        public override string Name
        {
            get { return "Blaster Cannon"; }
        }

        public override string ShortDescription
        {
            get { return "Primitive front mounted cannon"; }
        }

        public override Texture Image
        {
            get { return new Texture("shipparts", new RectangleI(64, 0, 32, 32)); }
        }

        public override string[] GenerateStatusText()
        {
            string[] ret = new string[] 
            {
                "Ammo Remaining: 420/600",
                "Ammo Type: projectile",
                "Rate of fire: 10/second",
                "Damage: low"
            };
            return ret;
        }

        int _coolDown = 0;
        internal override void Update()
        {
            if (_coolDown > 0)
                _coolDown--;
        }

        internal void Fire(Engine engine)
        {
            if (_coolDown > 0)
                return;
            _coolDown = 30;
            if (!Ship.DrawPower(1))
                return;

            Vector2 offset = new Vector2((InstallPoint.X - 1) * 0.2f, (InstallPoint.Y - 1) * 0.2f);
            offset = Vector2.Transform(offset, Matrix.CreateRotationZ(Ship.Rotation));
            Vector2 position = Ship.Position + offset;
            Vector2 direction = Ship.Facing;
            
            //position += Dice.RandomVector(0.15f);

            Particle particle = engine.Bullets.CreateBullet(position+direction, direction * 150);
            
            
            //Particle particle = engine.Shells.CreateShell(position, direction * 10);
            //particle.Body.ClearCollisionIgnores();
            //particle.Body.IgnoreCollisionWith(Ship.Body);


            //engine.System.Planets[0].Biosphere.AddCreature(Ship.Position + Dice.RandomVector2(5));
        }
    }
}
